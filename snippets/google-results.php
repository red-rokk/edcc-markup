<section class="site-search pad-5-bottom">
          <!-- SITE SEARCH -->

            <div class="row margin-2-top">
              <div class="col-xs-12">
                <h4>Search Results for Human Resources</h4>
                <hr class="hr-lg dark">
              </div>
            </div>

            <div class="row">
              <div class="col-xs-12">
                <form action="" method="POST" role="form">
                  <legend class="sr-only">Site Search</legend>
                  <div class="form-group">
                      <label for="site-searchfield" class="sr-only">Search the Site</label>
                      <input type="text" class="edcc-search form-control" id="site-searchfield" placeholder="Enter Search...">
                      <button type="submit" class="btn">
                        <i class="fa fa-search" aria-hidden="true">
                          <span class="sr-only">Submit Search</span>
                        </i>
                      </button>

                  </div>
                </form>
                <p>About 79,000 results</p>
                <hr class="hr-dark">
              </div>
            </div>
            <!-- ForEach Search Result goes on each row -->
            <div class="row search-result">
              <div class="col-xs-12">
                <a href="hr.edcc.edu/human-resources">
                  <h5>Edmonds Community College | Human Resources</h5>
                  <a href="hr.edcc.edu/human-resources" class="sub-link">hr.edcc.edu/human-resources</a>
                </a>
                <p class="margin-2-top margin-2-bottom">
                  Lorem ipsum dolor sit amet, vitae nunc a eget in, eu felis integer porta libero purus, sagittis consectetur nonummy, amet quam quisque accumsan turpis, malesuada et lorem sit felis. A pariatur tincidunt tincidunt nulla, lectus eros semper, elit cursus sem. Odio sociis. Vel vestibulum ligula. Tristique praesentium consectetu.
                </p>
                <hr class="hr-dark">
              </div>
            </div>
            <!-- ForEach Search Result goes on each row -->
            <div class="row search-result">
              <div class="col-xs-12">
                <a href="hr.edcc.edu/contact-us">
                  <h5>Contact Us | Human Resources</h5>
                  <a href="hr.edcc.edu/contact-us" class="sub-link">hr.edcc.edu/contact-us</a>
                </a>
                <p class="margin-2-top margin-2-bottom">
                  Lorem ipsum dolor sit amet, vitae nunc a eget in, eu felis integer porta libero purus, sagittis consectetur nonummy, amet quam quisque accumsan turpis, malesuada et lorem sit felis. A pariatur tincidunt tincidunt nulla, lectus eros semper, elit cursus sem. Odio sociis. Vel vestibulum ligula. Tristique praesentium consectetu.
                </p>
                <hr class="hr-dark">
              </div>
            </div>
            <!-- ForEach Search Result goes on each row -->
            <div class="row search-result">
              <div class="col-xs-12">
                <a href="hr.edcc.edu/vice-president">
                  <h5>Associate Vice President of Human Resources</h5>
                  <a href="hr.edcc.edu/vice-president" class="sub-link">hr.edcc.edu/vice-president</a>
                </a>
                <p class="margin-2-top margin-2-bottom">
                  Lorem ipsum dolor sit amet, vitae nunc a eget in, eu felis integer porta libero purus, sagittis consectetur nonummy, amet quam quisque accumsan turpis, malesuada et lorem sit felis. A pariatur tincidunt tincidunt nulla, lectus eros semper, elit cursus sem. Odio sociis. Vel vestibulum ligula. Tristique praesentium consectetu.
                </p>
                <hr class="hr-dark">
              </div>
            </div>
            <!-- ForEach Search Result goes on each row -->
            <div class="row search-result">
              <div class="col-xs-12">
                <a href="hr.edcc.edu/recruitment">
                  <h5>Recruitment | Human Resources</h5>
                  <a href="hr.edcc.edu/recruitment" class="sub-link">hr.edcc.edu/recruitment</a>
                </a>
                <p class="margin-2-top margin-2-bottom">
                  Lorem ipsum dolor sit amet, vitae nunc a eget in, eu felis integer porta libero purus, sagittis consectetur nonummy, amet quam quisque accumsan turpis, malesuada et lorem sit felis. A pariatur tincidunt tincidunt nulla, lectus eros semper, elit cursus sem. Odio sociis. Vel vestibulum ligula. Tristique praesentium consectetu.
                </p>
                <hr class="hr-dark">
              </div>
            </div>
            <!-- ForEach Search Result goes on each row -->
            <div class="row search-result">
              <div class="col-xs-12">
                <a href="hr.edcc.edu/forms/">
                  <h5>Forms and Processes | Human Resources</h5>
                  <a href="hr.edcc.edu/forms/" class="sub-link">hr.edcc.edu/forms/</a>
                </a>
                <p class="margin-2-top margin-2-bottom">
                  Lorem ipsum dolor sit amet, vitae nunc a eget in, eu felis integer porta libero purus, sagittis consectetur nonummy, amet quam quisque accumsan turpis, malesuada et lorem sit felis. A pariatur tincidunt tincidunt nulla, lectus eros semper, elit cursus sem. Odio sociis. Vel vestibulum ligula. Tristique praesentium consectetu.
                </p>
                <hr class="hr-dark">
              </div>
            </div>

          </section>