<section class="heading pad-3-top pad-2-bottom">
  <div class="container">
    <div class="row text-centered">
        <h3>WELCOMING. DIVERSE. INNOVATIVE.</h3>
        <hr class="hr-lg centered">
    </div>
  </div>
</section> 