
 
<?php include ('../headers/head.php'); ?>
<?php include ('../headers/header-template.php'); ?>  

<div class="wrapper homepage" id="wrapper-index">

<section class="heading pad-3-top pad-2-bottom">
  <div class="container">
    <div class="row text-centered">
        <h3>Article Image Demo</h3>
        <hr class="hr-lg centered">
    </div>
  </div>
</section> 


<!-- Breadcrumbs -->
<section class="breadcrumbs hidden-sm hidden-xs pad-1-both">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12">
				<nav class="nav">
				  <a class="nav-link" href="#"><i class="fa fa-home" aria-hidden="true"></i></a>
				  <a class="nav-link" href="#">Academics</a>
				  <a class="nav-link" href="#">Academic Departments</a>
				  <a class="nav-link active" href="#">Hospitality and Tourism</a>
				</nav>
			</div>
		</div>
	</div>
</section>


  <div class="container">
	<div class="row">
		<div class="two-third col-xs-12 col-sm-12 col-md-8 col-lg-8"> <!-- Two thirds Section-->
			

		<?php include ('../snippets/article-image.php'); ?>

		</div> <!-- EO Two thirds Section-->
		      

		<div class="one-third col-xs-12 col-sm-12 col-md-4 col-lg-4"> <!-- One third Section-->
			
		<?php include ('../template-sidebar.php'); ?>
			

		</div> <!-- EO One third Section-->

	</div>
</div>

</div><!-- Wrapper end -->

<?php include ('../footer.php'); ?>

