<section class="sidebar">

        <div class="post-box">
        <form action="" method="POST" role="form" class="search-form pad-8-bottom">
            <legend class="sr-only">Search Blog Posts</legend>
    
              <div class="input-group search-input-group">
                <label for="site-searchfield" class="sr-only">Search Blog</label>
                <input type="text" class="form-control"  placeholder="Search" >
                    <span class="input-group-addon">
                      <button type="submit">
                        <span class="glyphicon glyphicon-search"></span>
                        <span class="sr-only">Submit Search</span>
                      </button>  
                  </span>
              </div>

          </form>
          <div class="pad-3-bottom">
            <h5 class="w700">Recent Posts</h5>
            <hr class="hr-sm dark">
            <ul class="list-unstyled list-group">
              
              <li class="list-group-item"><a href="#">New Athletic Department</a></li>
              <li class="list-group-item"><a href="#">New Mural for Brier Hall</a></li>
              <li class="list-group-item"><a href="#">Registration Deadlines</a></li>
             <li class="list-group-item"><a href="#"> Reserved Faculty Parking</a></li>
            </ul>
          </div>
          <div class="pad-3-bottom">
            <h5 class="w700">Departments</h5>
            <hr class="hr-sm dark">
            <ul class="list-unstyled list-group">
              <li class="list-group-item"><a href="#">Athletic Department</a></li>
              <li class="list-group-item"><a href="#">Science Department</a></li>
              <li class="list-group-item"><a href="#">Technology Department</a></li>
              <li class="list-group-item"><a href="#">See all Departments</a></li>
            </ul>
          </div>
        </div>
</section>