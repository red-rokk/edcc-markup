
      <!-- Open Community Board -->
      <section class="open-community-board">
          <!-- page content -->
          <!-- Community Post -->
          <div class="row">
            <div class="xs-sm-12 posthero">
              <img src="../images/EDCC_Employee_Hero.jpg" class="img-responsive" alt="Responsive image">
              <div class="post-date">
                <h5 class="lighter">Mar</h5>
                <h4 class="lighter">08</h4>
              </div>
            </div>
          </div>

          <div class="row margin-4-top">
            <div class="xs-sm-12">
              <h4>New Mural For Briar Hall</h4>
              <hr class="hr-lg dark">
              <p>
                Lorem ipsum dolor sit amet, vitae nunc a eget in, eu felis integer porta libero purus, sagittis consectetur nonummy, amet quam quisque accumsan turpis, malesuada et lorem sit felis. A pariatur tincidunt tincidunt nulla, lectus eros semper, elit cursus sem. Odio sociis. Vel vestibulum ligula. Tristique praesentium consectetur eget. Sem commodo lorem sed sagittis et libero, vitae adipiscing non nonummy, lorem est nisl. Nec morbi. Adipiscing arcu massa rhoncus turpis quam wisi, nulla amet vivamus fringilla, sapien posuere ligula etiam suspendisse, phasellus amet hendrerit enim risus. Lorem ipsum dolor sit amet, vitae nunc a eget in, eu felis integer porta libero purus, sagittis consectetur nonummy, amet quam quisque accumsan turpis, malesuada et lorem sit felis. A pariatur tincidunt tincidunt nulla, lectus eros semper, elit cursus sem. Odio sociis. Vel vestibulum ligula. Tristique praesentium consectetur eget. Sem commodo lorem sed sagittis et libero, vitae adipiscing non nonummy, lorem est nisl. Nec morbi. Adipiscing arcu massa rhoncus turpis quam wisi, nulla amet vivamus fringilla, sapien posuere ligula etiam suspendisse, phasellus amet hendrerit enim risus.
              </p>
            </div>
          </div>

          <div class="row margin-5-top">
            <div class="xs-sm-12 author brand-bg-dkgrey">
              <h5>John Anderson</h5>
              <p class="date">February 28th, 2017</p>
              <br>
              <p>
                Lorem ipsum dolor sit amet, vitae nunc a eget in, eu felis integer porta libero purus, sagittis consectetur nonummy, amet quam quisque accumsan turpis, malesuada et lorem sit felis. A pariatur tincidunt tincidunt nulla, lectus eros semper, elit cursus sem. Odio sociis. Vel vestibulum ligula. Tristique praesentium consectetur eget. Sem commodo lorem sed sagittis et libero, vitae adipiscing non nonummy, lorem est nisl. Nec morbi. Adipiscing arcu massa rhoncus turpis quam wisi, nulla amet vivamus fringilla, sapien posuere ligula etiam suspendisse, phasellus amet hendrerit enim risus.
              </p>
              <br>
              <button type="button" class="btn-full-width btn btn-accent">Reply</button>

              <hr class="hr-dark margin-5-both">

              <div class="row">
                <div class="col-xs-12">
                  <a class="comments-toggle" role="button" data-toggle="collapse" href="#collapse_Comments" aria-expanded="false" aria-controls="collapseExample">
                    <span class="fa-stack fa-lg close" aria-expanded="false">
                      <i class="fa fa-circle-thin fa-stack-2x" aria-hidden="true"></i>
                      <i class="fa fa-plus fa-stack-1x" aria-hidden="true"></i>
                    </span>
                    <span class="fa-stack fa-lg open" aria-expanded="true">
                      <i class="fa fa-circle-thin fa-stack-2x" aria-hidden="true"></i>
                      <i class="fa fa-minus fa-stack-1x" aria-hidden="true"></i>
                    </span>
                    View All Comments
                  </a>
                  <div class="collapse" id="collapse_Comments">
                    <div class="well">
                      <h5>John Anderson</h5>
                      <p class="date pad-3-bottom">February 28th, 2017</p>
                      <p>
                        Lorem ipsum dolor sit amet, vitae nunc a eget in, eu felis integer porta libero purus, sagittis consectetur nonummy, amet quam quisque accumsan turpis, malesuada et lorem sit felis. A pariatur tincidunt tincidunt nulla, lectus eros semper, elit cursus sem. Odio sociis. Vel vestibulum ligula. Tristique praesentium consectetur eget. Sem commodo lorem sed sagittis et libero, vitae adipiscing non nonummy, lorem est nisl. Nec morbi. Adipiscing arcu massa rhoncus turpis quam wisi, nulla amet vivamus fringilla, sapien posuere ligula etiam suspendisse, phasellus amet hendrerit enim risus.
                      </p>
                    </div>
                    <div class="well">
                      <h5>John Anderson</h5>
                      <p class="date pad-3-bottom">February 28th, 2017</p>
                      <p>
                        Lorem ipsum dolor sit amet, vitae nunc a eget in, eu felis integer porta libero purus, sagittis consectetur nonummy, amet quam quisque accumsan turpis, malesuada et lorem sit felis. A pariatur tincidunt tincidunt nulla, lectus eros semper, elit cursus sem. Odio sociis. Vel vestibulum ligula. Tristique praesentium consectetur eget. Sem commodo lorem sed sagittis et libero, vitae adipiscing non nonummy, lorem est nisl. Nec morbi. Adipiscing arcu massa rhoncus turpis quam wisi, nulla amet vivamus fringilla, sapien posuere ligula etiam suspendisse, phasellus amet hendrerit enim risus.
                      </p>
                    </div>
                    <div class="well">
                      <h5>John Anderson</h5>
                      <p class="date pad-3-bottom">February 28th, 2017</p>
                      <p>
                        Lorem ipsum dolor sit amet, vitae nunc a eget in, eu felis integer porta libero purus, sagittis consectetur nonummy, amet quam quisque accumsan turpis, malesuada et lorem sit felis. A pariatur tincidunt tincidunt nulla, lectus eros semper, elit cursus sem. Odio sociis. Vel vestibulum ligula. Tristique praesentium consectetur eget. Sem commodo lorem sed sagittis et libero, vitae adipiscing non nonummy, lorem est nisl. Nec morbi. Adipiscing arcu massa rhoncus turpis quam wisi, nulla amet vivamus fringilla, sapien posuere ligula etiam suspendisse, phasellus amet hendrerit enim risus.
                      </p>
                    </div>
                    <div class="well">
                      <h5>John Anderson</h5>
                      <p class="date pad-3-bottom">February 28th, 2017</p>
                      <p>
                        Lorem ipsum dolor sit amet, vitae nunc a eget in, eu felis integer porta libero purus, sagittis consectetur nonummy, amet quam quisque accumsan turpis, malesuada et lorem sit felis. A pariatur tincidunt tincidunt nulla, lectus eros semper, elit cursus sem. Odio sociis. Vel vestibulum ligula. Tristique praesentium consectetur eget. Sem commodo lorem sed sagittis et libero, vitae adipiscing non nonummy, lorem est nisl. Nec morbi. Adipiscing arcu massa rhoncus turpis quam wisi, nulla amet vivamus fringilla, sapien posuere ligula etiam suspendisse, phasellus amet hendrerit enim risus.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row pad-6-top">
            <div class="xs-sm-12 comment brand-bg-dkgrey">
              <h5>Leave A Reply</h5>
                <form action="" method="POST" role="form" class="margin-4-top">
                  <legend class="sr-only">Leave a reply Form</legend>

                  <div class="form-group">
                    <label for="#comments" class="sr-only">Comment Field</label>
                    <textarea class="form-control" rows="5" id="comments" placeholder="Enter your comments here..."></textarea>

                    <div class="margin-4-both">
                      <p class="w600">Fill in your details below or click an icon to login:
                        <span class="pull-right">
                          <a href=""><i class="fa fa-facebook fa-fw fa-lg" aria-hidden="true"><span class="sr-only">Go to EDCC Facebood</span></i></a>
                          <a href=""><i class="fa fa-twitter fa-fw fa-lg" aria-hidden="true"><span class="sr-only">Go to EDCC Twitter</span></i></a>
                          <a href=""><i class="fa fa-google-plus fa-fw fa-lg" aria-hidden="true"><span class="sr-only">Go to EDCC Google Plus</span></i></a>
                        </span>
                      </p>
                    </div>

                    <label for="#name" class="sr-only">Name Field</label>
                    <input type="text" class="form-control" id="name" placeholder="Enter your name here...">

                    <label for="#email" class="sr-only">Email Field</label>
                    <input type="text" class="form-control margin-1-top" id="email" placeholder="Enter your email here...">

                    <div class="checkbox">
                      <label>
                        <input type="checkbox"><p class="w600">Notify me of follow-up comments via email</p>
                      </label>
                    </div>

                  </div>
                  <br>

                  <button type="button" class="btn-full-width btn btn-accent">Comment</button>

                </form>
            </div>
          </div>


      </section>

