<section class="breadcrumbs hidden-sm hidden-xs">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12">
				<nav class="nav">
				  <a class="nav-link" href="#"><i class="fa fa-home" aria-hidden="true"><span class="sr-only">EDCC Home</span></i></a>
				  <a class="nav-link" href="#">Academics</a>
				  <a class="nav-link" href="#">Academic Departments</a>
				  <a class="nav-link active" href="#">Hospitality and Tourism</a>
				</nav>
			</div>
		</div>
	</div>
</section>