<section class="sidebar margin-6-bottom">

		<div class="one-fourth">
			<div class="sidebar-underlay">
	    		<img src="/images/EDCC_Image4_students.png">
	    	</div>

			<div class="sidebar-text">
				<i class="text-centered lighter fa fa-file-text-o margin-4-bottom" aria-hidden="true"></i>
				<h4 class="pad-4-both lighter">SUBMIT A POST<br><span class="thin">TO THE COMMUNITY BOARD</span></h4>
				<div class="pos-bottom text-centered">
					<a href="#" type="button" class="margin-3-top btn btn-trans w500">Submit</a>
				</div>

			</div>

		</div>
		<div class="sidebar-button">
			<a href="#" type="button" class="btn-full margin-3-both btn btn-secondary">View All Posts</a>
		</div>

</section>