<div class="collapse navbar-collapse navbar-ex3-collapse">

  <ul class="nav navbar-nav">
      <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
      <li class="hidden active">
          <a></a> 
      </li>
      <li class="nav-item dropdown visible-xs">

          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Quick Links</a>
          <ul class="dropdown-menu">

            <div class="row multicol-nav">
                <ul class="list-unstyled col-md-6">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a class="" href="#"></a>
                </li>
                  <li class="nav-item">
                      <a class="" href="/index.php"><i class="fa fa-home" aria-hidden="true"></i> Main</a>
                  </li>
                  <li class="nav-item">
                      <a class="" href="/students.php"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Students</a>
                  </li>
                  <li class="nav-item">
                      <a class="" href="/employee.php"><i class="fa fa-briefcase" aria-hidden="true"></i> Employees</a>
                  </li>
                  <li class="nav-item">
                      <a class="" href="#"><i class="fa fa-globe" aria-hidden="true"></i> International</a>
                  </li>
                </ul>
            </div>

          </ul>
      </li>
      <li class="nav-item dropdown">

          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">I am</a>

          <ul class="dropdown-menu">

            <div class="row multicol-nav">
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Alumni</a></li>
                   <li><a class="dropdown-item" href="#">Current Students</a></li>
                   <li><a class="dropdown-item" href="#">Donor</a></li>
                   <li><a class="dropdown-item" href="#">Employer/Business</a></li>
                   <li><a class="dropdown-item" href="#">Faculty or Staff</a></li>

                </ul>
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Future Student</a></li>
                   <li><a class="dropdown-item" href="#">High School Student</a></li>
                   <li><a class="dropdown-item" href="#">International Student</a></li>
                   <li><a class="dropdown-item" href="#">Parent of a Student</a></li>
                   <li><a class="dropdown-item" href="#">Visitors and Community</a></li>
                </ul>
            </div>

          </ul>
      </li>

      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">About</a>
          <ul class="dropdown-menu">

            <div class="row multicol-nav">
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Alumni</a></li>
                   <li><a class="dropdown-item" href="#">Current Students</a></li>
                   <li><a class="dropdown-item" href="#">Donor</a></li>
                   <li><a class="dropdown-item" href="#">Employer/Business</a></li>
                   <li><a class="dropdown-item" href="#">Faculty or Staff</a></li>

                </ul>
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Future Student</a></li>
                   <li><a class="dropdown-item" href="#">High School Student</a></li>
                   <li><a class="dropdown-item" href="#">International Student</a></li>
                   <li><a class="dropdown-item" href="#">Parent of a Student</a></li>
                   <li><a class="dropdown-item" href="#">Visitors and Community</a></li>
                </ul>
            </div>

          </ul>
      </li>
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Academics</a>
          <ul class="dropdown-menu">

            <div class="row multicol-nav">
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Alumni</a></li>
                   <li><a class="dropdown-item" href="#">Current Students</a></li>
                   <li><a class="dropdown-item" href="#">Donor</a></li>
                   <li><a class="dropdown-item" href="#">Employer/Business</a></li>
                   <li><a class="dropdown-item" href="#">Faculty or Staff</a></li>

                </ul>
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Future Student</a></li>
                   <li><a class="dropdown-item" href="#">High School Student</a></li>
                   <li><a class="dropdown-item" href="#">International Student</a></li>
                   <li><a class="dropdown-item" href="#">Parent of a Student</a></li>
                   <li><a class="dropdown-item" href="#">Visitors and Community</a></li>
                </ul>
            </div>

          </ul>
      </li>
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Enroll</a>
          <ul class="dropdown-menu">

            <div class="row multicol-nav">
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Alumni</a></li>
                   <li><a class="dropdown-item" href="#">Current Students</a></li>
                   <li><a class="dropdown-item" href="#">Donor</a></li>
                   <li><a class="dropdown-item" href="#">Employer/Business</a></li>
                   <li><a class="dropdown-item" href="#">Faculty or Staff</a></li>

                </ul>
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Future Student</a></li>
                   <li><a class="dropdown-item" href="#">High School Student</a></li>
                   <li><a class="dropdown-item" href="#">International Student</a></li>
                   <li><a class="dropdown-item" href="#">Parent of a Student</a></li>
                   <li><a class="dropdown-item" href="#">Visitors and Community</a></li>
                </ul>
            </div>

          </ul>
      </li>
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Triton Life</a>
          <ul class="dropdown-menu">

            <div class="row multicol-nav">
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Alumni</a></li>
                   <li><a class="dropdown-item" href="#">Current Students</a></li>
                   <li><a class="dropdown-item" href="#">Donor</a></li>
                   <li><a class="dropdown-item" href="#">Employer/Business</a></li>
                   <li><a class="dropdown-item" href="#">Faculty or Staff</a></li>

                </ul>
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Future Student</a></li>
                   <li><a class="dropdown-item" href="#">High School Student</a></li>
                   <li><a class="dropdown-item" href="#">International Student</a></li>
                   <li><a class="dropdown-item" href="#">Parent of a Student</a></li>
                   <li><a class="dropdown-item" href="#">Visitors and Community</a></li>
                </ul>
            </div>

          </ul>
      </li>
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">News &amp; Events</a>
          <ul class="dropdown-menu">

            <div class="row multicol-nav">
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Alumni</a></li>
                   <li><a class="dropdown-item" href="#">Current Students</a></li>
                   <li><a class="dropdown-item" href="#">Donor</a></li>
                   <li><a class="dropdown-item" href="#">Employer/Business</a></li>
                   <li><a class="dropdown-item" href="#">Faculty or Staff</a></li>

                </ul>
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Future Student</a></li>
                   <li><a class="dropdown-item" href="#">High School Student</a></li>
                   <li><a class="dropdown-item" href="#">International Student</a></li>
                   <li><a class="dropdown-item" href="#">Parent of a Student</a></li>
                   <li><a class="dropdown-item" href="#">Visitors and Community</a></li>
                </ul>
            </div>

          </ul>
      </li>
      <li class="hidden-xs nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search" aria-hidden="true"></i></a>
          <ul class="dropdown-menu search-menu">
            <div class="row multicol-nav">
              <form role="search" method="get" id="searchform" action="" _lpchecked="1">
                  <div class="input-group">
                      <label for="s" class="hidden">Search </label>
                      <input type="text" class="field form-control" name="s" id="s" placeholder="Search …">
                      <span class="input-group-btn">
                          <label for="searchsubmit" class="hidden">Search Submit</label>
                          <input type="submit" class="submit btn btn-primary" name="submit" id="searchsubmit" value="Search">
                      </span>
                   </div>
              </form>
            </div>

          </ul>
      </li>

      <li class="visible-xs nav-item">
          <a class="brand-item-em text-centered" href="#">Apply</a>
      </li>
  </ul>
</div>