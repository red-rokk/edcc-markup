<section class="login-hero pad-4-bottom">

  <div class="jumbotron brand-overlay-dark">

  <div class="container stats-container">
    <div class="row">

      <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="landing-left">
	        <img class="img-responsive" src="/images/EDCC_Main_Logo.png" alt="EDCC Logo">
	        <h1 class="lighter pad-4-top">Registration<br><span>Login</span></h1>
	        <hr class="hr-lg light">
	    </div>
	   </div>

	   <div class="col-sm-12 col-md-6 col-lg-6">
	     <div class="landing-right">
	     		<p class="lighter">Select the Quarter/Year you are registering for:</p>
	     	<form class="student-login-form">
	     		<div class="form-group selectdiv">
            <label for="quarter" class="sr-only">Select Quarter</label>
				    <select placeholder="Select Quarter..." class="form-control login" id="quarter">
				      <option>Winter 2017</option>
				      <option>Spring 2017</option>
				      <option>Summer 2017</option>
				      <option>Winter 2018</option>
				    </select>
				</div>
		     	<div class="form-group">
            <label for="idnumber" class="sr-only">Enter Student ID Number</label>
		     		<input type="text" class="form-control login text-centered" placeholder="Enter your student ID number" id="idnumber">
		     	</div>
		     	<div class="form-group">
             <label for="pin" class="sr-only">Enter PIN</label>
		     		<input type="number" class="form-control login text-centered" placeholder="Enter your PIN" id="pin">
		     	</div>

		     	<button type="submit" class="btn btn-primary text-centered" id="login">Login</button>

	     	</form>
	     </div>
	    </div>

    </div>
  </div>

    <img class="simple-image" src="/images/EDCC_BG_RegistrationLogin.jpg">

  </div>

</section>