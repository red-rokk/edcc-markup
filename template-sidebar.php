
<!-- Main Template Sidebar -->

<section class="sidebar">
    
    <div class="post-box">
      <div class="pad-3-bottom">
        <h5>Hero Banners</h5>
        <hr class="hr-sm dark">
        <ul class="list-unstyled list-group">
          <li class="list-group-item"><a href="/template-parts/homepage.php">Home Page</a></li>
          <li class="list-group-item"><a href="/template-parts/students-page.php">Students Page</a></li>
          <li class="list-group-item"><a href="/template-parts/employees-page.php">Employees Page</a></li>
          <li class="list-group-item"><a href="/template-parts/students-internal.php">Students Internal</a></li>
          <li class="list-group-item"><a href="/template-parts/employees-internal.php">Employees Internal</a></li>
          <li class="list-group-item"><a href="/template-parts/registration.php">Registration Login</a></li>
         
        </ul>
      </div>
      <div class="pad-3-bottom">
        <h5>Full Width Page</h5>
        <hr class="hr-sm dark">
        <ul class="list-unstyled list-group">
          <li class="list-group-item"><a href="/template-parts/centered-title-primary.php">Centered Title Primary</a></li>
          <li class="list-group-item"><a href="/template-parts/centered-title-secondary.php">Centered Title Secondary</a></li>
          <li class="list-group-item"><a href="/template-parts/statistics.php">Statistics Section</a></li>
          <li class="list-group-item"><a href="/template-parts/image-grid.php">Image Grid</a></li>
          <li class="list-group-item"><a href="/template-parts/tables.php">Tables</a></li>
          <li class="list-group-item"><a href="/template-parts/testimonial.php">Testimonial Section</a></li>
          <li class="list-group-item"><a href="/template-parts/news-events.php">News &amp; Events</a></li>
          <li class="list-group-item"><a href="/template-parts/dates-events.php">Dates &amp; Events</a></li>
          <li class="list-group-item"><a href="/template-parts/featured-cards.php">Featured Cards</a></li>
         
        </ul>
      </div>
      <div class="pad-3-bottom">
        <h5>Sidebar Page</h5>
        <hr class="hr-sm dark">
        <ul class="list-unstyled list-group">
          <li class="w900 list-group-item">2/3 Boxed Container</li>
          <li class="list-group-item"><a href="/template-parts/community-board-loop.php">Community Board Loop</a></li>
          <li class="list-group-item"><a href="/template-parts/community-board-internal.php">Community Board Internal</a></li>
          <li class="list-group-item"><a href="/template-parts/contact-bio.php">Contact Bio List</a></li>
          <li class="list-group-item"><a href="/template-parts/accordion.php">Accordion</a></li>
          <li class="list-group-item"><a href="/template-parts/article-image.php">Article Image</a></li>
          <li class="list-group-item"><a href="/template-parts/article-video.php">Article Video</a></li>
          <li class="list-group-item"><a href="/template-parts/google-results.php">Google Search Results</a></li>
          <li class="list-group-item"><a href="/template-parts/class-results.php">Class Search Results</a></li>
          <li class="w900 list-group-item">1/3 Sidebar Container</li>
          <li class="list-group-item"><a href="/template-parts/filter-list.php">Filter List</a></li>
          <li class="list-group-item"><a href="/template-parts/filter-search.php">Filter Search</a></li>
          <li class="list-group-item"><a href="/template-parts/filter-button.php">Filter Button</a></li>
          <li class="list-group-item"><a href="/template-parts/cta-ad.php">Call to Action Ad</a></li>
          <li class="list-group-item"><a href="/template-parts/facebook-feed.php">Facebook Feed</a></li>
          <li class="list-group-item"><a href="/template-parts/sharing-icons.php"></a>Sharing Icons</li>

       
        </ul>
      </div>
      
    </div>
     
</section>