
<!DOCTYPE html>
<html>

<head>
<title>EDCC HTML/CSS Markup</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="favicon.ico">

<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="/css/custom.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="/css/custom-two.css" rel="stylesheet" type="text/css" media="screen"/>
<link rel="stylesheet" href="/css/ie10-viewport-bug-workaround.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="/css/font-awesome.css" type="text/css" media="screen"/>
<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,900" rel="stylesheet">
<link rel="shortcut icon" type="image/png" href="/wp-content/uploads/2017/01/hipfavicon.png"/>

</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=667619243291882";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="page" class="site font-root">