<section class="news-events">

  <div class="container">
    <div class="row">

      <div class="col-sm-12 col-md-6">
        <article class="news row">
          <div class="col-sm-12 col-md-6">
            <div class="article-image">
              <img alt="Place first news image description here" src="/images/EDCC_Main_NewsImage1.jpg">
            </div>
          </div>
          <div class="col-sm-12 col-md-6 article-content">
            <h5>Preparing for Graduation this spring</h5>
            <hr class="hr-sm">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut do eiusmod  incididunt ulabore...</p>
            <a href="#" class="pad-2-both readmore">Read More</a>
          </div>
        </article>

        <article class="news row">
          <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="article-image">
              <img alt="Place second news image description here" src="/images/EDCC_Main_NewsImage2.jpg">
            </div>
          </div>
          <div class="col-sm-12 col-md-6 article-content">
            <h5>Preparing for Graduation this spring</h5>
            <hr class="hr-sm">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut do eiusmod  incididunt ulabore...</p>
            <a href="#" class="margin-3-both readmore">Read More</a>
          </div>
        </article>
      </div>

      <div class="col-sm-12 col-md-6 col-lg-5 col-lg-offset-1">
        <article class="events">
          <ul class="list-unstyled event-list-group">

            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                      <a href="#">
                        <h5>Edmonds Soccer Game</h5>
                      </a>
                      <ul class="list-unstyled group-details">
                        <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                        <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                      </ul>
                  </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                      <a href="#">
                        <h5>Edmonds Soccer Game</h5>
                      </a>
                      <ul class="list-unstyled group-details">
                        <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                        <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                      </ul>
                  </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                      <a href="#">
                        <h5>Edmonds Soccer Game</h5>
                      </a>
                      <ul class="list-unstyled group-details">
                        <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                        <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                      </ul>
                  </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                      <a href="#">
                        <h5>Edmonds Soccer Game</h5>
                      </a>
                      <ul class="list-unstyled group-details">
                        <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                        <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                      </ul>
                  </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                      <a href="#">
                        <h5>Edmonds Soccer Game</h5>
                      </a>
                      <ul class="list-unstyled group-details">
                        <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                        <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                      </ul>
                  </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                      <a href="#">
                        <h5>Edmonds Soccer Game</h5>
                      </a>
                      <ul class="list-unstyled group-details">
                        <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                        <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                      </ul>
                  </div>
                </div>
              </div>
            </li>



          </ul>
        </article>

        <div class="article-button margin-4-top pad-3-both">
          <a href="#" class="btn btn-tertiary">All News and Events</a>
        </div>

      </div>

    </div>
  </div>


</section>