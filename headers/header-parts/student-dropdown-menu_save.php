<div class="collapse navbar-collapse navbar-ex3-collapse">

  <ul class="nav navbar-nav">
      <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
      <li class="hidden active">
          <a></a>
      </li>
      <li class="nav-item dropdown">
          
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">I am</a>

          <ul class="dropdown-menu">
             
            <div class="row multicol-nav">
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Alumni</a></li>
                   <li><a class="dropdown-item" href="#">Current Students</a></li>
                   <li><a class="dropdown-item" href="#">Donor</a></li>
                   <li><a class="dropdown-item" href="#">Employer/Business</a></li>
                   <li><a class="dropdown-item" href="#">Faculty or Staff</a></li>

                </ul>       
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Future Student</a></li>
                   <li><a class="dropdown-item" href="#">High School Student</a></li>
                   <li><a class="dropdown-item" href="#">International Student</a></li>
                   <li><a class="dropdown-item" href="#">Parent of a Student</a></li>
                   <li><a class="dropdown-item" href="#">Visitors and Community</a></li>
                </ul>
            </div>
             
          </ul>
      </li>

      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">About</a>
          <ul class="dropdown-menu">
             
            <div class="row multicol-nav">
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Alumni</a></li>
                   <li><a class="dropdown-item" href="#">Current Students</a></li>
                   <li><a class="dropdown-item" href="#">Donor</a></li>
                   <li><a class="dropdown-item" href="#">Employer/Business</a></li>
                   <li><a class="dropdown-item" href="#">Faculty or Staff</a></li>

                </ul>       
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Future Student</a></li>
                   <li><a class="dropdown-item" href="#">High School Student</a></li>
                   <li><a class="dropdown-item" href="#">International Student</a></li>
                   <li><a class="dropdown-item" href="#">Parent of a Student</a></li>
                   <li><a class="dropdown-item" href="#">Visitors and Community</a></li>
                </ul>
            </div>
             
          </ul>
      </li>
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Academics</a>
          <ul class="dropdown-menu">
             
            <div class="row multicol-nav">
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Alumni</a></li>
                   <li><a class="dropdown-item" href="#">Current Students</a></li>
                   <li><a class="dropdown-item" href="#">Donor</a></li>
                   <li><a class="dropdown-item" href="#">Employer/Business</a></li>
                   <li><a class="dropdown-item" href="#">Faculty or Staff</a></li>

                </ul>       
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Future Student</a></li>
                   <li><a class="dropdown-item" href="#">High School Student</a></li>
                   <li><a class="dropdown-item" href="#">International Student</a></li>
                   <li><a class="dropdown-item" href="#">Parent of a Student</a></li>
                   <li><a class="dropdown-item" href="#">Visitors and Community</a></li>
                </ul>
            </div>
             
          </ul>
      </li>
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Enroll</a>
          <ul class="dropdown-menu">
             
            <div class="row multicol-nav">
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Alumni</a></li>
                   <li><a class="dropdown-item" href="#">Current Students</a></li>
                   <li><a class="dropdown-item" href="#">Donor</a></li>
                   <li><a class="dropdown-item" href="#">Employer/Business</a></li>
                   <li><a class="dropdown-item" href="#">Faculty or Staff</a></li>

                </ul>       
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Future Student</a></li>
                   <li><a class="dropdown-item" href="#">High School Student</a></li>
                   <li><a class="dropdown-item" href="#">International Student</a></li>
                   <li><a class="dropdown-item" href="#">Parent of a Student</a></li>
                   <li><a class="dropdown-item" href="#">Visitors and Community</a></li>
                </ul>
            </div>
             
          </ul>
      </li>
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Triton Life</a>
          <ul class="dropdown-menu">
             
            <div class="row multicol-nav">
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Alumni</a></li>
                   <li><a class="dropdown-item" href="#">Current Students</a></li>
                   <li><a class="dropdown-item" href="#">Donor</a></li>
                   <li><a class="dropdown-item" href="#">Employer/Business</a></li>
                   <li><a class="dropdown-item" href="#">Faculty or Staff</a></li>

                </ul>       
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Future Student</a></li>
                   <li><a class="dropdown-item" href="#">High School Student</a></li>
                   <li><a class="dropdown-item" href="#">International Student</a></li>
                   <li><a class="dropdown-item" href="#">Parent of a Student</a></li>
                   <li><a class="dropdown-item" href="#">Visitors and Community</a></li>
                </ul>
            </div>
             
          </ul> 
      </li>
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">News &amp; Events</a>
          <ul class="dropdown-menu">
             
            <div class="row multicol-nav">
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Alumni</a></li>
                   <li><a class="dropdown-item" href="#">Current Students</a></li>
                   <li><a class="dropdown-item" href="#">Donor</a></li>
                   <li><a class="dropdown-item" href="#">Employer/Business</a></li>
                   <li><a class="dropdown-item" href="#">Faculty or Staff</a></li>

                </ul>       
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="#">Future Student</a></li>
                   <li><a class="dropdown-item" href="#">High School Student</a></li>
                   <li><a class="dropdown-item" href="#">International Student</a></li>
                   <li><a class="dropdown-item" href="#">Parent of a Student</a></li>
                   <li><a class="dropdown-item" href="#">Visitors and Community</a></li>
                </ul>
            </div>
             
          </ul>
      </li>
      <li class="hidden-xs nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search" aria-hidden="true"></i></a>
          <ul class="dropdown-menu search-menu">
            <div class="row multicol-nav">
              <form role="search" method="get" id="searchform" action="" _lpchecked="1">
                  <div class="input-group">
                      <input type="text" class="field form-control" name="s" id="s" placeholder="Search …">
                      <span class="input-group-btn">
                          <input type="submit" class="submit btn btn-primary" name="submit" id="searchsubmit" value="Search">
                      </span>
                   </div>
              </form>
            </div>

          </ul>
      </li>
      <li class="visible-xs nav-item xs-search">    
        <form role="search" method="get" id="searchform" action="" _lpchecked="1">
            <div class="input-group">
                <input type="text" class="field form-control" name="s" id="s" placeholder="Search …">
             </div>
        </form>   
      </li>

  </ul>
</div>