<section class="text-block pad-4-bottom">
  <div class="container">
    <div class="row text-centered">
      <div class="col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
    <p class="text-centered">Whether you want to start prepping for a career you love, interact with new cultures, or transfer to a four-year university—your future starts here. Sprawled across a 50-acre campus in Lynnwood, WA, EdCC offers 61 associate degrees and 63 professional certificates taught by world-class instructors who truly embrace our mission of strengthening our diverse community through innovation, service, and lifelong learning. Acquire new skills, make new friends, and set a strong foundation for a successful future: learn more about EdCC today.</p>
      <a href="#" class="margin-4-both btn btn-primary btn-block-mobile-only">Explore Edmonds</a>
    </div>
  </div>
</section>