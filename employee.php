<?php include ('headers/head.php'); ?>
<?php include ('headers/header-employee.php'); ?>

<div class="wrapper employees-page" id="wrapper-index">



<section class="students-hero pad-4-bottom pad-0-top">

  <div class="jumbotron overlay-60">
    <img class="simple-image" src="/images/EDCC_Employee_Hero.jpg">

    <div class="container relative heading-centered">
        <div class="stu-overlay-content">
	        <div class="row head-comp">
	          <h1 class="text-centered lighter">Faculty &amp; Staff <span class="accent">Insider</span></h1>

	          <h2 class="margin-3-top margin-4-bottom text-centered lighter">Welcome to your student portal</h2>
	          <div class="text-centered student-quicklinks pad-4-both">
		          <div class="col-sm-12 col-md-4">
		          	<a href="#"><button class="btn btn-accent" type="button">Email Login</button></a>
		          </div>
		          <div class="col-sm-12 col-md-4">
		          	<a href="#"><button class="btn btn-accent" type="button">TLR Login</button></a>
		          </div>
		          <div class="col-sm-12 col-md-4">
		          	<a href="#"><button class="btn btn-accent" type="button">Employee Training</button></a>
		          </div>

	          </div>
	        </div>
       	</div>

    </div>
  </div>

</section>


<section class="heading pad-1-top pad-3-bottom">
  <div class="container">
    <div class="row text-centered">
        <h3>Community Board</h3>
        <hr class="hr-lg centered">
    </div>
  </div>
</section>

<div class="container">
  <div class="row">
    <div class="two-third col-xs-12 col-sm-12 col-md-8 col-lg-8"> <!-- Two thirds Section-->
      <?php include ('snippets/community-board-loop.php'); ?>
    </div>

    <div class="one-third col-xs-12 col-sm-12 col-md-4 col-lg-4"> <!-- One third Section-->
      <?php include ('snippets/sidebar/sidebar-submit-post.php'); ?>

    </div>
  </div>
</div>


<section class="section-feature pad-6-top pad-3-bottom">
  <div class="feature-container">

    <div class="hidden-lg hidden-md culture-image">
        <img class="" src="/images/EDCC_Main_TestimonialImage.jpg">
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-4">
            </div>

            <div class="col-sm-12 col-md-6 col-lg-8">
                <div class="culture-call-right">
                    <h3 class="lighter">Employee <span>Spotlight</span></h3>
                    <hr class="hr-lg light">
                    <p class="lighter quote">“Edmonds Community College prepared me for a successful career in the culinary arts. Within a year of graduating, I became the head chef at one of Seattle’s top restaurants.”
                    <em>--Amanda Smith, EdCC Business Graduate</em>
                    </p>
                </div>
            </div>
        </div>
    </div>
  </div>
</section>

<section class="heading pad-3-top pad-4-bottom">
  <div class="container">
    <div class="row text-centered">
        <h3>News and Events</h3>
        <hr class="hr-lg centered">
    </div>
  </div>
</section>

<section class="news-events">

  <div class="container">
    <div class="row">

      <div class="col-sm-12 col-md-6">
        <article class="news row">
          <div class="col-sm-12 col-md-6">
            <div class="article-image">
              <img alt="Place First Image Description here" src="/images/EDCC_Main_NewsImage1.jpg">
            </div>
          </div>
          <div class="col-sm-12 col-md-6 article-content">
            <h5>Preparing for Graduation this spring</h5>
            <hr class="hr-sm">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut do eiusmod  incididunt ulabore...</p>
            <a href="#" class="pad-2-both readmore">Read More</a>
          </div>
        </article>

        <article class="news row">
          <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="article-image">
              <img alt="Place Second Image Description here" src="/images/EDCC_Main_NewsImage2.jpg">
            </div>
          </div>
          <div class="col-sm-12 col-md-6 article-content">
            <h5>Preparing for Graduation this spring</h5>
            <hr class="hr-sm">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut do eiusmod  incididunt ulabore...</p>
            <a href="#" class="margin-3-both readmore">Read More</a>
          </div>
        </article>
      </div>

      <div class="col-sm-12 col-md-6 col-lg-5 col-lg-offset-1">
        <article class="events">
          <ul class="list-unstyled event-list-group">

            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                          <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                        </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                          <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                        </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                          <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                        </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                          <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                        </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                          <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                        </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                          <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                        </ul>
                    </div>
                </div>
              </div>
            </li>



          </ul>
        </article>

        <div class="article-button margin-4-top pad-3-both">
          <a href="#" class="btn btn-tertiary">All News and Events</a>
        </div>

      </div>

    </div>
  </div>


</section>




</div>

<?php include ('footer.php'); ?>


