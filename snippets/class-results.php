<!-- Class Search results -->
 
      <!-- page content -->
      <section class="class-search">


          <!-- Community Post -->
          <div class="results-course">
            
            <!-- Course -->
            
            <div class="row margin-4-bottom no-gutter">
              
              <div class="col-xs-12 title">
                <div class="brand-bg-secondary margin-3-bottom">
                  <h5 class="lighter">Acct 101 - Accounting</h5>
                </div>
              </div>

              <!-- Class -->
              <div class="row results-class">
                
                <div class="col-xs-12 col-sm-3">
                  <div class="num">
                    <h5 class="lighter margin-0-bottom">0304</h5>
                    <hr class="hr-sm">
                  </div>
                </div>
                <div class="col-xs-12 col-sm-9 details">
                  <h6>SECTION A: KRISTOF-NE D</h6>
                  <p class="list-search">9:30 AM - 10:20 AM | MTWTH</p>
                  <p class="list-search margin-2-top">5 Credits | Hybrid | Online | <em class="open">Class is Open</em></p>
                  <p class="list-search margin-2-top">Class Location: <span>MDL-0228</span></p>
                  <p class="list-search margin-2-top">Hybrid course. Permit Code Requires. Contact I-BEST: IBEST@edcc.edu or 425.640.1628. This is an IBEST lass. An additional fee of $31.55 is collected for this course. <a class="read-more" href="">Register&nbsp;Now</a></p>
                </div>
              </div>

              <hr class="hr-ltdark">
              <!-- Class -->
              <div class="row results-class">
                <div class="col-xs-12 col-sm-3">
                  <div class="num">
                    <h5 class="lighter margin-0-bottom">0305</h5>
                    <hr class="hr-sm">
                  </div>
                </div>
                <div class="col-xs-12 col-sm-9 details">  
                  <h6>SECTION A: KRISTOF-NE D</h6>
                  <p class="list-search margin-2-top">9:30 AM - 10:20 AM | MTWTH</p>
                  <p class="list-search margin-2-top">5 Credits | Hybrid | Online | <em class="closed">Class is Closed</em></p>
                  <p class="list-search margin-2-top">Class Location: <span>MDL-0228</span></p>
                  <p class="list-search margin-2-top">Hybrid course. Permit Code Requires. Contact I-BEST: IBEST@edcc.edu or 425.640.1628. This is an IBEST lass. An additional fee of $31.55 is collected for this course. <a class="read-more" href="">Register&nbsp;Now</a></p>
                </div>
              </div>

              <hr class="hr-ltdark">

            </div>



            <!-- Course -->
            <div class="row margin-4-bottom no-gutter">
              <div class="col-xs-12 title">
                <div class="brand-bg-secondary margin-3-bottom">
                  <h5 class="lighter">Acct 201 - Accounting</h5>
                </div>
              </div>
              <!-- Class -->
              <div class="row results-class">
                <div class="col-xs-12 col-sm-3">
                  <div class="num">
                    <h5 class="lighter margin-0-bottom">0306</h5>
                    <hr class="hr-sm">
                  </div>
                </div>

                <div class="col-xs-12 col-sm-9 details">  
                  <h6>SECTION A: KRISTOF-NE D</h6>
                  <p class="list-search margin-2-top">9:30 AM - 10:20 AM | MTWTH</p>
                  <p class="list-search margin-2-top">5 Credits | Hybrid | Online | <em class="open">Class is Open</em></p>
                  <p class="list-search margin-2-top">Class Location: <span>MDL-0228</span></p>
                  <p class="list-search margin-2-top">Hybrid course. Permit Code Requires. Contact I-BEST: IBEST@edcc.edu or 425.640.1628. This is an IBEST lass. An additional fee of $31.55 is collected for this course. <a class="read-more" href="">Register&nbsp;Now</a></p>
                </div>
              </div>
              <hr class="hr-ltdark">
            </div>
          </div>

  
      </section>