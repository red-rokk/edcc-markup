
 
<?php include ('../headers/head.php'); ?>
<?php include ('../headers/header-template.php'); ?>  

<div class="wrapper homepage" id="wrapper-index">

<section class="heading pad-3-top pad-2-bottom">
  <div class="container">
    <div class="row text-centered">
        <h3>Google Search Results Demo</h3>
        <hr class="hr-lg centered">
    </div>
  </div>
</section> 


  <div class="container">
	<div class="row">
		<div class="two-third col-xs-12 col-sm-12 col-md-8 col-lg-8"> <!-- Two thirds Section-->
			

		<?php include ('../snippets/google-results.php'); ?>

		</div> <!-- EO Two thirds Section-->
		      

		<div class="one-third col-xs-12 col-sm-12 col-md-4 col-lg-4"> <!-- One third Section-->
			
		<?php include ('../template-sidebar.php'); ?>
			

		</div> <!-- EO One third Section-->

	</div>
</div>

</div><!-- Wrapper end -->

<?php include ('../footer.php'); ?>

