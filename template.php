
 
<?php include ('headers/head.php'); ?>
<?php include ('headers/header-template.php'); ?>  

<div class="wrapper homepage" id="wrapper-index">

	<div class="container margin-6-top margin-2-bottom">
		<h4>This is the Main Kitchen Sink File</h4>
	  	<ul class="pad-3-top">
		  	<li><p>Use the Dropdown menu to filter through all available template sections.</p></li>
		  	<li><p>We'll be using the Main Homepage Navigation menu for this demonstration.</p></li>
		  	<li><p>To view the other two navigation menus select the Students or Employees tabs in the upper navigation.</p></li>
		  	<li><p>The Compiled Sink will display all sections in a one page format.</p></li>
	  </ul>


      <a class="btn btn-primary margin-3-both" href="./sink.php">View Compiled Kitchen Sink</a>
  
	 </div>
  	

</div><!-- Wrapper end -->

<?php include ('footer.php'); ?>

