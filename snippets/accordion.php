<section class="accordion pad-6-both">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<div class="article-content">
					        <h4>Human Resources: Quick Links</h4>
					        <hr class="hr-lg dark">

					        <div class="panel-group" id="accordion">

							  <div class="panel panel-default">
							    <a class="accordion-link" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true">
							    	<div class="panel-heading">
							      		<h4 class="lighter panel-title">ASSOCIATE VP OF HUMAN RESOURCES
							      			<i class="fa fa-plus" aria-hidden="true"><span class="sr-only">Expand Content</span></i>
							      			<i class="fa fa-minus" aria-hidden="true"><span class="sr-only">Collapse Content</span></i>
							      		</h4>
								    </div>
							    </a>
							    <div id="collapse1" class="panel-collapse collapse in">
							      <div class="panel-body">
							      	<ul>
										<li>Public Records Requests</li>
										<li>Title IX Coordinator</li>
										<li>Employee Relations</li>
										<li>Collective Bargaining Agreements for Faculty/
										Classified Employees</li>
										<li>Investigation and Resolution of Complaints</li>
									</ul>
							      </div>
							    </div>
							  </div>

							  <div class="panel panel-default">
							    <a class="accordion-link" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false">
							    	<div class="panel-heading">
							      		<h4 class="lighter panel-title">ASSOCIATE VP OF HUMAN RESOURCES
							      			<i class="fa fa-plus" aria-hidden="true"><span class="sr-only">Expand Content</span></i>
							      			<i class="fa fa-minus" aria-hidden="true"><span class="sr-only">Collapse Content</span></i>
							      		</h4>
								    </div>
							    </a>
							    <div id="collapse2" class="panel-collapse collapse">
							      <div class="panel-body">
							      	<ul>
										<li>Public Records Requests</li>
										<li>Title IX Coordinator</li>
										<li>Employee Relations</li>
										<li>Collective Bargaining Agreements for Faculty/
										Classified Employees</li>
										<li>Investigation and Resolution of Complaints</li>
									</ul>
							      </div>
							    </div>
							  </div>

							  <div class="panel panel-default">
							    <a class="accordion-link" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false">
							    	<div class="panel-heading">
							      		<h4 class="lighter panel-title">ASSOCIATE VP OF HUMAN RESOURCES
							      			<i class="fa fa-plus" aria-hidden="true"><span class="sr-only">Expand Content</span></i>
							      			<i class="fa fa-minus" aria-hidden="true"><span class="sr-only">Collapse Content</span></i>
							      		</h4>
								    </div>
							    </a>
							    <div id="collapse3" class="panel-collapse collapse">
							      <div class="panel-body">
							      	<ul>
										<li>Public Records Requests</li>
										<li>Title IX Coordinator</li>
										<li>Employee Relations</li>
										<li>Collective Bargaining Agreements for Faculty/
										Classified Employees</li>
										<li>Investigation and Resolution of Complaints</li>
									</ul>
							      </div>
							    </div>
							  </div>
							  <div class="panel panel-default">
							    <a class="accordion-link" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false">
							    	<div class="panel-heading">
							      		<h4 class="lighter panel-title">ASSOCIATE VP OF HUMAN RESOURCES
							      			<i class="fa fa-plus" aria-hidden="true"><span class="sr-only">Expand Content</span></i>
							      			<i class="fa fa-minus" aria-hidden="true"><span class="sr-only">Collapse Content</span></i>
							      		</h4>
								    </div>
							    </a>
							    <div id="collapse4" class="panel-collapse collapse">
							      <div class="panel-body">
							      	<ul>
										<li>Public Records Requests</li>
										<li>Title IX Coordinator</li>
										<li>Employee Relations</li>
										<li>Collective Bargaining Agreements for Faculty/
										Classified Employees</li>
										<li>Investigation and Resolution of Complaints</li>
									</ul>
							      </div>
							    </div>
							  </div>
							  <div class="panel panel-default">
							    <a class="accordion-link" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false">
							    	<div class="panel-heading">
							      		<h4 class="lighter panel-title">ASSOCIATE VP OF HUMAN RESOURCES
							      			<i class="fa fa-plus" aria-hidden="true"><span class="sr-only">Expand Content</span></i>
							      			<i class="fa fa-minus" aria-hidden="true"><span class="sr-only">Collapse Content</span></i>
							      		</h4>
								    </div>
							    </a>
							    <div id="collapse5" class="panel-collapse collapse">
							      <div class="panel-body">
							      	<ul>
										<li>Public Records Requests</li>
										<li>Title IX Coordinator</li>
										<li>Employee Relations</li>
										<li>Collective Bargaining Agreements for Faculty/
										Classified Employees</li>
										<li>Investigation and Resolution of Complaints</li>
									</ul>
							      </div>
							    </div>
							  </div>
							  <div class="panel panel-default">
							    <a class="accordion-link" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false">
							    	<div class="panel-heading">
							      		<h4 class="lighter panel-title">ASSOCIATE VP OF HUMAN RESOURCES
							      			<i class="fa fa-plus" aria-hidden="true"><span class="sr-only">Expand Content</span></i>
							      			<i class="fa fa-minus" aria-hidden="true"><span class="sr-only">Collapse Content</span></i>
							      		</h4>
								    </div>
							    </a>
							    <div id="collapse6" class="panel-collapse collapse">
							      <div class="panel-body">
							      	<ul>
										<li>Public Records Requests</li>
										<li>Title IX Coordinator</li>
										<li>Employee Relations</li>
										<li>Collective Bargaining Agreements for Faculty/
										Classified Employees</li>
										<li>Investigation and Resolution of Complaints</li>
									</ul>
							      </div>
							    </div>
							  </div>

							</div>


					    </div>
					</div>
				</div>
			</section>