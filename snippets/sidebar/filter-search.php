<section class="sidebar">

        <div class="post-box">
        <form action="" method="POST" role="form" class="search-form pad-8-bottom">
            <legend class="sr-only">Search Blog Posts</legend>
    
              <div class="input-group search-input-group">
                <label for="site-searchfield" class="sr-only">Search Blog</label>
                <input type="text" class="form-control"  placeholder="Search" >
                    <span class="input-group-addon">
                      <button type="submit">
                        <span class="glyphicon glyphicon-search"></span>
                        <span class="sr-only">Submit Search</span>
                      </button>  
                  </span>
              </div>

          </form>

          
        </div>

      </section>