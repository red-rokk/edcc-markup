<section class="employee-hero pad-4-bottom pad-0-top">

  <div class="jumbotron overlay-60">
    <img class="simple-image" src="/images/EDCC_Employee_Hero.jpg">
    
    <div class="container relative heading-centered">
        <div class="stu-overlay-content">
	        <div class="row head-comp">
	          <h1 class="text-centered lighter">Faculty &amp; Staff <span class="accent">Insider</span></h1>

	          <h2 class="margin-3-top margin-4-bottom text-centered lighter">Welcome to your student portal</h2>
	               
	        </div> 
       	</div>     
       
    </div>      
  </div>

</section>