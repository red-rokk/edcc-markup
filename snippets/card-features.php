<section class="card-features">

	<div class="container">
		<div class="row">

			<div class="col-sm-12 col-md-12 col-lg-4">
				<div class="article-content pad-8-top">
					<div class="image-container margin-6-bottom">
						<img alt="Place first image description here" src="/images/EDCC_3xGrid_Image3.jpg">
					</div>
					<h5>Certificates</h5>
					<hr class="hr-sm dark">
					<p>Our world-renound 44.5 credit requirement, Hospitality and Tourism Certificate provides you with a positive job outlook. This certificate will provide you with the necessary skills to be a as Reservation and Transportation Ticket Agent, Travel Clerk, or Flight Attendant.</p>
				<a href="#" class="pad-5-top sub-link">Program Requirements<br>See Job Outlook and Salaries</a>
				</div>
			</div>

			<div class="col-sm-12 col-md-12 col-lg-4">
				<div class="article-content pad-8-top">
					<div class="image-container margin-6-bottom">
						<img alt="Place second image description here" src="/images/EDCC_3xgrid_Image2.jpg">
					</div>
					<h5>2-year degrees</h5>
					<hr class="hr-sm dark">
					<p>Our world-renound 44.5 credit requirement, Hospitality and Tourism Certificate provides you with a positive job outlook. This certificate will provide you with the necessary skills to be a as Reservation and Transportation Ticket Agent, Travel Clerk, or Flight Attendant.</p>
				<a href="#" class="pad-5-top sub-link">Program Requirements<br>See Job Outlook and Salaries</a>
				</div>
			</div>

			<div class="col-sm-12 col-md-12 col-lg-4">
				<div class="article-content pad-8-top">
					<div class="image-container margin-6-bottom">
						<img alt="Place third image description here" src="/images/EDCC_3xGrid_Image1.jpg">
					</div>
					<h5>Pathways to 4-year degrees</h5>
					<hr class="hr-sm dark">
					<p>Our world-renound 44.5 credit requirement, Hospitality and Tourism Certificate provides you with a positive job outlook. This certificate will provide you with the necessary skills to be a as Reservation and Transportation Ticket Agent, Travel Clerk, or Flight Attendant.</p>
				<a href="#" class="pad-5-top sub-link">Program Requirements<br>See Job Outlook and Salaries</a>
				</div>
			</div>


		</div>
	</div>

</section>