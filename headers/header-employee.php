    

<!--Header Employee-->

<header class="navbar-fixed-top header-employee">
    <!--Navbar-->


<nav class="navbar navbar-fixed-top" role="navigation">
    <a href="#wrapper-index" class="sr-only">Skip Navigation to Page Content</a>

    <div class="offcanvas-navbar brand-bg-secondary second fixed-load hidden-md-down">
        <div class="container">
            <div class="row no-gutter row-offcanvas">
                <div class="navbar-header page-scroll">

                    <div class="container-fluid visible-xs">
                        <div class="row text-centered margin-4-top">
                            <div class="col-xs-12">
                                <a class="navbar-brand" href="#">
                                    <img src="/images/EDCC_Main_Logo_color.png" alt="EDCC Home Page">
                                </a>
                            </div>

                        </div>
                    </div>
                    <a class="navbar-toggle edcc-mobile" data-toggle="offcanvas"><i class="fa fa-cog" aria-hidden="true"></i> My EDCC <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                </div>

                <div class="collapse navbar-collapse navbar-ex2-collapse">
                    <ul class="pull-left nav navbar-nav">
                        <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                        <li class="hidden">
                            <a class="" href="#"></a>
                        </li>
                        <li class="nav-item">
                            <a class="" href="/index.php"><i class="fa fa-home" aria-hidden="true"></i> Main</a>
                        </li>
                        <li class="nav-item">
                            <a class="" href="/students.php"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Students</a>
                        </li>
                        <li class="nav-item active">
                            <a class="" href="/employee.php"><i class="fa fa-briefcase" aria-hidden="true"></i> Employees</a>
                        </li>
                        <li class="nav-item">
                            <a class="" href="#"><i class="fa fa-globe" aria-hidden="true"></i> International</a>
                        </li>
                    </ul>


                    <ul class="pull-right nav navbar-nav">
                        <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                        <li class="hidden active">
                            <a class="page-scroll" href="#"></a>
                        </li>
                        <li class="nav-item">
                            <a class="brand-item-em text-centered" href="#">Canvas</a>
                        </li>
                        <li class="nav-item">
                            <a class="myedcc-mobile" data-toggle="offcanvas"><i class="brand-color-tertiary fa fa-cog" aria-hidden="true"></i> My EDCC  <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

    <div class="logo-navbar second fixed-load hidden-xs">
        <div class="container">

            <div class="navbar-header page-scroll">

                <a class="navbar-brand" href="#">
                    <img src="/images/EDCC_Main_Logo_color.png" alt="EDCC Home Page">
                </a>
            </div>
        </div>
    </div>
    <div id="collapseSearch" class="collapse">
        <div class="container">
            <form role="search" method="get" id="searchform" action="" _lpchecked="1">

              <div class="input-group">

                  <label for="s" class="hidden">Search:</label>
                  <input type="text" class="field form-control" name="s" id="s" placeholder="Search …">
                  <span class="input-group-btn">
                      <label for="searchsubmit" class="hidden">Submit Search:</label>
                      <input type="submit" class="submit btn btn-primary" name="submit" id="searchsubmit" value="Search">
                  </span>
              </div>
          </form>
        </div>
    </div>


    <div class="dropdown-navbar second fixed-load">
        <div class="container">
            <div class="row no-gutter row-offcanvas">
                <div class="col-xs-6 visible-xs">
               <a class="nav-link dropdown-toggle" data-toggle="collapse" href="#collapseSearch" role="button" aria-controls="#collapseSearch" aria-expanded="false"><i class="fa fa-search fa-lg" aria-hidden="true"></i></a>
            </div>
            <div class="col-xs-6 visible-xs">
                  <div class="navbar-header page-scroll">
                    <h5>Menu</h5>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex3-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

            </div>

                <?php include('header-parts/employee-dropdown-menu.php'); ?>

            </div>
        </div>
    </div>


</nav>


</header>


<?php include ('off_canvas_nav.php'); ?>





