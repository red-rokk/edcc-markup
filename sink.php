
<?php include ('headers/head.php'); ?>
<?php include ('headers/header-main.php'); ?>

<div class="wrapper homepage" id="wrapper-index">

<!-- Registration / Login Banner -->
<?php include ('snippets/registration.php'); ?>



<!-- Centered Title -->
<?php include ('snippets/centered-title-primary.php'); ?>
<!-- Sidebar Page -->
<div class="container">
	<div class="row">
		<div class="two-third col-xs-12 col-sm-12 col-md-8 col-lg-8"> <!-- Two thirds Section-->
			<!-- Open Community Board -->
			<div class="pad-8-bottom"><?php include ('snippets/community-board-loop.php'); ?></div>
		</div>

		<div class="one-third col-xs-12 col-sm-12 col-md-4 col-lg-4"> <!-- One third Section-->
			<!-- Sidebar Filter List -->
			<?php include ('snippets/sidebar/filter-list.php'); ?>
		</div>
	</div>
</div>



<!-- Centered Title -->
<?php include ('snippets/centered-title-secondary.php'); ?>
<!-- Sidebar Page -->
<div class="container">
	<div class="row">
		<div class="two-third col-xs-12 col-sm-12 col-md-8 col-lg-8"> <!-- Two thirds Section-->
			<!-- Community Board Internal -->
			<div class="pad-8-bottom"><?php include ('snippets/community-board-internal.php'); ?></div>
		</div>

		<div class="one-third col-xs-12 col-sm-12 col-md-4 col-lg-4"> <!-- One third Section-->
			<!-- Sidebar Community -->
			<?php include ('snippets/sidebar/sidebar-community.php'); ?>
			<!-- Sidebar Call to Action -->
			<?php include ('snippets/sidebar/sidebar-submit-post.php'); ?>
		</div>
	</div>
</div>



<!-- Centered Title -->
<?php include ('snippets/centered-title-primary.php'); ?>
<!-- Sidebar Page -->
<div class="container">
	<div class="row">

		<div class="two-third col-xs-12 col-sm-12 col-md-8 col-lg-8"> <!-- Two thirds Section-->
			<!-- Article Image -->
			<div class="pad-8-bottom"><?php include ('snippets/article-image.php'); ?></div>
			<!-- Accordion -->
			<div class="pad-8-bottom"><?php include ('snippets/accordion.php'); ?></div>
		</div>

		<div class="one-third col-xs-12 col-sm-12 col-md-4 col-lg-4"> <!-- One third Section-->
			<!-- Sidebar Button -->
			<?php include ('snippets/sidebar/filter-button.php'); ?>
			<!-- Sidebar Call to Action -->
			<?php include ('snippets/sidebar/facebook-feed.php'); ?>
		</div>
	</div>
</div>



<!-- Centered Title -->
<?php include ('snippets/centered-title-secondary.php'); ?>
<!-- Sidebar Page -->
<div class="container">
	<div class="row">

		<div class="two-third col-xs-12 col-sm-12 col-md-8 col-lg-8"> <!-- Two thirds Section-->
			<!-- Google Search -->
			<div class="pad-8-bottom"><?php include ('snippets/google-results.php'); ?></div>
		</div>

		<div class="one-third col-xs-12 col-sm-12 col-md-4 col-lg-4"> <!-- One third Section-->
			<!-- Sidebar Button -->
			<?php include ('snippets/sidebar/filter-button.php'); ?>
			<!-- Sidebar Sharing Icons -->
			<?php include ('snippets/sidebar/sharing-icons.php'); ?>
		</div>
	</div>
</div>



<!-- Centered Title -->
<?php include ('snippets/centered-title-primary.php'); ?>
<!-- Sidebar Page -->
<div class="container">
	<div class="row">

		<div class="two-third col-xs-12 col-sm-12 col-md-8 col-lg-8"> <!-- Two thirds Section-->
			<!-- Class Results -->
			<div class="pad-8-bottom"><?php include ('snippets/class-results.php'); ?></div>
		</div>

		<div class="one-third col-xs-12 col-sm-12 col-md-4 col-lg-4"> <!-- One third Section-->
			<!-- Sidebar Button -->
			<?php include ('snippets/sidebar/filter-list.php'); ?>
		</div>
	</div>
</div>

	

<!-- Centered Title -->
<?php include ('snippets/centered-title-secondary.php'); ?>
<!-- Cards -->	
<div class="pad-4-bottom"><?php include ('snippets/card-features.php'); ?></div>
<!-- Student Banner -->
<div class="pad-4-top"><?php include ('snippets/students-banner.php'); ?></div>

<!-- Centered Title -->
<div class="pad-2-top"><?php include ('snippets/centered-title-primary.php'); ?></div>
<!-- Text Block -->
<?php include ('snippets/text-block-centered.php'); ?>
<!-- Stats Banner -->
<?php include ('snippets/stats-banner.php'); ?>



<!-- Centered Title -->
<?php include ('snippets/centered-title-primary.php'); ?>
<!-- Image Grid -->	
<?php include ('snippets/image-grid.php'); ?>

<!-- Employees Banner -->	
<div class="pad-4-top"><?php include ('snippets/employees-banner.php'); ?></div>


<!-- Centered Title -->
<?php include ('snippets/centered-title-primary.php'); ?>
<!-- Dates Events -->
<?php include ('snippets/dates-events.php'); ?>
<!-- Testimonials -->
<?php include ('snippets/testimonial.php'); ?>
<!-- Centered Title -->
<?php include ('snippets/centered-title-primary.php'); ?>
<!-- News Events  -->
<?php include ('snippets/news-events.php'); ?>




</div>
<!-- Footer -->
<?php include ('footer.php'); ?>