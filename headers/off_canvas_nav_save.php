<section class="myedcc">

  <div class="row row-offcanvas row-offcanvas-right">
      <div class="col-xs-6 col-sm-6 sidebar-offcanvas" id="sidebar">
          <h5 class="lighter">MYEDCC TEST</h5>
          <h4 class="lighter">TOOLKIT</h4>
          <hr class="hr-lg lighter">
            <ul class="side-group">
              <li><a href="#" class="hidden side-group-item active"></a></li>
              <li><a href="#" class="side-group-item"><i class="fa fa-cog" aria-hidden="true"></i> Customize MYEDCC</a></li>
        <li><a href="#" class="side-group-item"><i class="fa fa-calendar" aria-hidden="true"></i> Academic Calendar</a></li>
        <li><a href="#" class="side-group-item"><i class="fa fa-files-o" aria-hidden="true"></i> A-Z Index</a></li>
        <li><a href="#" class="side-group-item"><i class="fa fa-paint-brush" aria-hidden="true"></i> Canvas</a></li>
        <li><a href="#" class="side-group-item"><i class="fa fa-clock-o" aria-hidden="true"></i> Class Schedule</a></li>
        <li><a href="#" class="side-group-item"><i class="fa fa-envelope" aria-hidden="true"></i> Ed Mail</a></li>
        <li><a href="#" class="side-group-item"><i class="fa fa-book" aria-hidden="true"></i> Library</a></li>
        <li><a href="#" class="side-group-item"><i class="fa fa-check-square-o" aria-hidden="true"></i> Register for Classes</a></li>
        <li><a href="#" class="side-group-item"><i class="fa fa-file-o" aria-hidden="true"></i> Transcripts &amp; Grades</a></li>
        </ul>
      </div>
    </div><!--/.sidebar-offcanvas-->


</section>