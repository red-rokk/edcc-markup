<section class="hero image-hero pad-2-both">

  <div class="jumbotron brand-overlay-dark">

  <div class="container stats-container">
    <div class="row">

      <div class="col-sm-12 col-md-6 col-lg-3 text-centered stats-item">
        <h2>11,100+</h2>
        <h5 class="lighter">EDMONDS CC<br>STUDENTS</h5>
      </div>

      <div class="col-sm-12 col-md-6 col-lg-3 text-centered stats-item">
        <h2>1,500+</h2>
        <h5 class="lighter">International<br>Students</h5>
      </div>

      <div class="col-sm-12 col-md-6 col-lg-3 text-centered stats-item">
        <h2>$17M+</h2>
        <h5 class="lighter">Financial Aid<br>Awards</h5>
      </div>

      <div class="col-sm-12 col-md-6 col-lg-3 text-centered stats-item">
        <h2>$3,708</h2>
        <h5 class="lighter">Est. YEARLY<br>TUITION AND FEES</h5>
      </div>

    </div>
  </div>
    <img class="simple-image" src="/images/EDCC_Stats_BGimage.jpg">
  </div>

</section>