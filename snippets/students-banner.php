<section class="students-hero pad-4-bottom pad-0-top">

  <div class="jumbotron overlay-40">
    <img class="simple-image" src="/images/EDCC_Hero_StudentSite.jpg" alt="EDCC Students" >

    <div class="container relative heading-centered">
        <div class="stu-overlay-content">
	        <div class="row head-comp">
	          <h1 class="text-centered lighter">The Mighty, Mighty <span>Tritons</span></h1>

	          <h2 class="margin-3-top margin-4-bottom text-centered lighter">Welcome to your student portal</h2>
	          <div class="text-centered student-quicklinks pad-4-both">
		          <div class="col-sm-12 col-md-4">
		          	<a href="#"><button class="btn btn-secondary" type="button">Login to Canvas</button></a>
		          </div>
		          <div class="col-sm-12 col-md-4">
		          	<a href="#"><button class="btn btn-secondary" type="button">Check ED MAil</button></a>
		          </div>
		          <div class="col-sm-12 col-md-4">
		          	<a href="#"><button class="btn btn-secondary" type="button">Register for classes</button></a>
		          </div>

	          </div>
	        </div>
       	</div>

    </div>
  </div>

</section>