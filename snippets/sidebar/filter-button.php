<section class="sidebar">
        <div class="post-box">
          <div class="pad-3-bottom">
            <h5>Resources</h5>
            <hr class="hr-sm dark">
            <ul class="list-unstyled list-group">

              <li class="list-group-item"><a href="#">How To Read Class Listings</a></li>
              <li class="list-group-item"><a href="#">Continuous Enrollment</a></li>
              <li class="list-group-item"><a href="#">Late Start</a></li>
              <li class="list-group-item"><a href="#">Bus Passes</a></li>
              <li class="list-group-item"><a class="margin-4-top btn btn-primary btn-block" href="#">New Search</a></li>
             
            </ul>
          </div>
        </div>
      </section>