
<!--Footer-->
<footer class="page-footer pad-12-top center-on-small-only">
    <div class="container-fluid footer-color">
    <!--Footer Links-->
      <div class="container footer-padding">
        <div class="row">

            <!--First column-->
            <div class="col-sm-12 col-md-4 col-lg-2">
                <h5 class="title lighter">About</h5>
                <hr class="hr-sm light">
                    <ul class="list-unstyled list-group">
                         <li class="list-group-item"><a href="">About EDCC</li>
                         <li class="list-group-item"><a href="">Diversity</li>
                         <li class="list-group-item"><a href="">Job Opportunities</li>
                         <li class="list-group-item"><a href="">Leadership</li>
                         <li class="list-group-item"><a href="">Mission</li>
                         <li class="list-group-item"><a href="">Quick Facts</li>
                    </ul>
            </div>
            <!--/.First column-->

            <!--Second column-->
            <div class="col-sm-12 col-md-4 col-lg-2">
                <h5 class="title lighter">Contact</h5>
                <hr class="hr-sm light">
                    <ul class="list-unstyled list-group">
                         <li class="list-group-item"><a href="">Contact Us</a></li>
                         <li class="list-group-item"><a href="">Driving Directions</a></li>
                         <li class="list-group-item"><a href="">425.640.1459</a></li>
                         <li class="list-group-item"><a href="">info@edcc.edu</a></li>
                    </ul>
            </div>
            <!--/.Second column-->

            <!--Third column-->
            <div class="col-sm-12 col-md-4 col-lg-2">
                <h5 class="title lighter">On Campus</h5>
                <hr class="hr-sm light">
                    <ul class="list-unstyled list-group">
                         <li class="list-group-item"><a href="">Campus Maps</a></li>
                         <li class="list-group-item"><a href="">Campus Resources</a></li>
                         <li class="list-group-item"><a href="">Office Hours</a></li>
                         <li class="list-group-item"><a href="">Parking</a></li>
                    </ul>
            </div>
            <!--/.Third column-->

            <!--Fourth column-->
            <div class="col-sm-12 col-md-4 col-lg-2">
                <h5 class="title lighter">Policies</h5>
                <hr class="hr-sm light">
                    <ul class="list-unstyled list-group">
                         <li class="list-group-item"><a href="">Non-Discrimination</a></li>
                         <li class="list-group-item"><a href="">Privacy</a></li>
                         <li class="list-group-item"><a href="">Safety and Security</a></li>
                         <li class="list-group-item"><a href="">Student Right to Know</a></li>
                    </ul>
            </div>
            <!--/.Fourth column-->

            <div class="col-sm-12 col-md-4 col-lg-1 vertical-rule">
                <div class="vr visible-lg-block"></div>
            </div>
            <!--Fifth column-->

            <div class="col-sm-12 col-md-4 col-lg-3">
                <h5 class="title lighter">Give</h5>
                <hr class="hr-sm light">
                    <ul class="list-unstyled list-group">
                         <li class="list-group-item"><a href="">EDCC Foundation</a></li>
                    </ul>

                <div class="block-button">
                  <a href="#"><button class="btn btn-primary" type="button">Donate Now</button></a>
              </div>

            </div>
            <!--/.Fifth column-->
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="language-button">
                  <a href="#"><button class="btn btn-tertiary" type="button">Select a language</button></a>
              </div>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-6">
                <ul class="list-inline social-inline">
                  <li class="list-inline-item">
                    <a target="_blank" href="#">
                      <span class="sr-only">Go to EDCC Facebook</span>
                      <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                      </span>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a target="_blank" href="#">
                      <span class="sr-only">Go to EDCC Twitter</span>
                      <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                      </span>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a target="_blank" href="#">
                      <span class="sr-only">Go to EDCC LinkeIn</span>
                      <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
                      </span>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a target="_blank" href="#">
                      <span class="sr-only">Go to EDCC Google plus</span>
                      <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
                      </span>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a target="_blank" href="#">
                      <span class="sr-only">Go to EDCC Instagram</span>
                      <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                      </span>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a target="_blank" href="#">
                      <span class="sr-only">Go to EDCC YouTube</span>
                      <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-youtube fa-stack-1x fa-inverse"></i>
                      </span>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a target="_blank" href="#">
                      <span class="sr-only">Go to EDCC Pinterest</span>
                      <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-pinterest fa-stack-1x fa-inverse"></i>
                      </span>
                    </a>
                  </li>

                </ul>
            </div>
        </div>


      </div>
    </div>


    <!--/.Footer Links-->

    <!--Copyright-->
    <div class="footer-copyright">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-6 brand-container">
                       <div class="brand-logo">
                           <img src="/images/EDCC_Main_Logo.png">
                       </div>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-6 copyright">
                        © 2017 Copyright EDCC All Rights Reserved | <a href="#">Sitemap</a>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!--Copyright-->

</footer>
<!--Footer-->


</div><!-- #page -->



<!-- SCRIPTS -->
<?php include ('scripts.php'); ?>


</body>

</html>