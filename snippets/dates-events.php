<section class="news-events pad-2-top pad-4-bottom">

  <div class="container">
    <div class="row">

      <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="row text-centered title margin-2-bottom brand-bg-accent">
          <div class="col-xs-12">
            <h4>Dates</h4>
          </div>
        </div>

        <article class="dates">
          <ul class="list-unstyled event-list-group">

            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                    <a href="#">
                      <h5>Edmonds Soccer Game</h5>
                    </a>
                    <ul class="list-unstyled group-details">
                      <li class="details-item"><i class="fa fa-calendar" aria-hidden="true"></i> Last day to withdraw, add a class, or change credit status
                      </li>

                      </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                    <a href="#">
                      <h5>Edmonds Soccer Game</h5>
                    </a>
                    <ul class="list-unstyled group-details">
                      <li class="details-item"><i class="fa fa-calendar" aria-hidden="true"></i> Last day to withdraw, add a class, or change credit status
                      </li>

                      </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                    <a href="#">
                      <h5>Edmonds Soccer Game</h5>
                    </a>
                    <ul class="list-unstyled group-details">
                      <li class="details-item"><i class="fa fa-calendar" aria-hidden="true"></i> Last day to withdraw, add a class, or change credit status
                      </li>

                      </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                    <a href="#">
                      <h5>Edmonds Soccer Game</h5>
                    </a>
                    <ul class="list-unstyled group-details">
                      <li class="details-item"><i class="fa fa-calendar" aria-hidden="true"></i> Last day to withdraw, add a class, or change credit status
                      </li>

                      </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                    <a href="#">
                      <h5>Edmonds Soccer Game</h5>
                    </a>
                    <ul class="list-unstyled group-details">
                      <li class="details-item"><i class="fa fa-calendar" aria-hidden="true"></i> Last day to withdraw, add a class, or change credit status
                      </li>

                      </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                    <a href="#">
                      <h5>Edmonds Soccer Game</h5>
                    </a>
                    <ul class="list-unstyled group-details">
                      <li class="details-item"><i class="fa fa-calendar" aria-hidden="true"></i> Last day to withdraw, add a class, or change credit status
                      </li>

                      </ul>
                    </div>
                </div>
              </div>
            </li>

          </ul>
        </article>
      </div>
      <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="row text-centered title margin-2-bottom brand-bg-primary">
          <div class="col-xs-12">
            <h4>Events</h4>
          </div>
        </div>

        <article class="events">
          <ul class="list-unstyled event-list-group">

            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                    <a href="#">
                      <h5>Edmonds Soccer Game</h5>
                    </a>
                    <ul class="list-unstyled group-details">
                      <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                      <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                    </ul>
                  </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                    <a href="#">
                      <h5>Edmonds Soccer Game</h5>
                    </a>
                    <ul class="list-unstyled group-details">
                      <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                      <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                    </ul>
                  </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                    <a href="#">
                      <h5>Edmonds Soccer Game</h5>
                    </a>
                    <ul class="list-unstyled group-details">
                      <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                      <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                    </ul>
                  </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                    <a href="#">
                      <h5>Edmonds Soccer Game</h5>
                    </a>
                    <ul class="list-unstyled group-details">
                      <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                      <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                    </ul>
                  </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                    <a href="#">
                      <h5>Edmonds Soccer Game</h5>
                    </a>
                    <ul class="list-unstyled group-details">
                      <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                      <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                    </ul>
                  </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                  <div class="event-content">
                    <a href="#">
                      <h5>Edmonds Soccer Game</h5>
                    </a>
                    <ul class="list-unstyled group-details">
                      <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                      <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                    </ul>
                  </div>
                </div>
              </div>
            </li>



          </ul>
        </article>

      </div>

    </div>
  </div>


</section>