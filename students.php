<?php include ('headers/head.php'); ?>
<?php include ('headers/header-student.php'); ?>

<div class="wrapper students-page" id="wrapper-index">


<section class="students-hero pad-4-bottom pad-0-top">

  <div class="jumbotron overlay-40">
    <img class="simple-image" src="/images/EDCC_Hero_StudentSite.jpg">

    <div class="container relative heading-centered">
        <div class="stu-overlay-content">
	        <div class="row head-comp">
	          <h1 class="text-centered lighter">The Mighty, Mighty <span>Tritons</span></h1>

	          <h2 class="margin-3-top margin-4-bottom text-centered lighter">Welcome to your student portal</h2>
	          <div class="text-centered student-quicklinks pad-4-both">
		          <div class="col-sm-12 col-md-4">
		          	<a href="#"><button class="btn btn-secondary" type="button">Login to Canvas</button></a>
		          </div>
		          <div class="col-sm-12 col-md-4">
		          	<a href="#"><button class="btn btn-secondary" type="button">Check ED MAil</button></a>
		          </div>
		          <div class="col-sm-12 col-md-4">
		          	<a href="#"><button class="btn btn-secondary" type="button">Register for classes</button></a>
		          </div>

	          </div>
	        </div>
       	</div>

    </div>
  </div>

</section>


<section class="heading pad-3-top pad-3-bottom">
  <div class="container">
    <div class="row text-centered">
        <h3>Important Dates and events</h3>
        <hr class="hr-lg centered">
    </div>
  </div>
</section>

<section class="news-events pad-2-top pad-4-bottom">

  <div class="container">
    <div class="row">

      <div class="col-sm-12 col-md-6 col-lg-6">
       	<article class="dates">
          <ul class="list-unstyled event-list-group">

            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-calendar" aria-hidden="true"></i> Last day to withdraw, add a class, or change credit status
                          </li>

                        </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-calendar" aria-hidden="true"></i> Last day to withdraw, add a class, or change credit status
                          </li>

                        </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-calendar" aria-hidden="true"></i> Last day to withdraw, add a class, or change credit status
                          </li>

                        </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-calendar" aria-hidden="true"></i> Last day to withdraw, add a class, or change credit status
                          </li>

                        </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-calendar" aria-hidden="true"></i> Last day to withdraw, add a class, or change credit status
                          </li>

                        </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-calendar" aria-hidden="true"></i> Last day to withdraw, add a class, or change credit status
                          </li>

                        </ul>
                    </div>
                </div>
              </div>
            </li>

          </ul>
        </article>
      </div>

      <div class="col-sm-12 col-md-6 col-lg-6">
        <article class="events">
          <ul class="list-unstyled event-list-group">

            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                          <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                        </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                          <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                        </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                          <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                        </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                          <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                        </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                          <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                        </ul>
                    </div>
                </div>
              </div>
            </li>
            <li class="event-item">
              <div class="row">
                <div class="xxs-full col-xs-4 col-sm-4 col-md-3">
                    <div class="event-date">
                      <h5 class="lighter">Mar</h5>
                      <h4 class="lighter">08</h4>
                    </div>
                </div>
                <div class="xxs-full col-xs-8 col-sm-8 col-md-9">
                    <div class="event-content">
                      <h5>Edmonds Soccer Game</h5>
                        <ul class="list-unstyled group-details">
                          <li class="details-item"><i class="fa fa-clock-o" aria-hidden="true"></i> 8:00 pm - 10:00 pm</li>
                          <li class="details-item"><i class="fa fa-map-marker" aria-hidden="true"></i> Edmonds Field</li>
                        </ul>
                    </div>
                </div>
              </div>
            </li>



          </ul>
        </article>

      </div>

    </div>
  </div>


</section>

<section class="section-feature pad-5-top pad-3-bottom">
  <div class="feature-container">

    <div class="hidden-lg hidden-md culture-image">
        <img class="" src="/images/EDCC_TestimonialImage_StudentSite.png">
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-4">
            </div>

            <div class="col-sm-12 col-md-6 col-lg-8">
                <div class="culture-call-right">
                    <h3 class="lighter">STUDENTS:<br><span>THE PLACE TO BE</span></h3>
                    <hr class="hr-lg light">
                    <p class="lighter quote">“Edmonds Community College prepared me for a successful career in the culinary arts. Within a year of graduating, I became the head chef at one of Seattle’s top restaurants.”
                    <em>--Amanda Smith, EdCC Business Graduate</em>
                    </p>
                </div>
            </div>
        </div>
    </div>
  </div>
</section>

<section class="heading pad-2-top pad-2-bottom">
  <div class="container">
    <div class="row text-centered">
        <h3>Get Involved</h3>
        <hr class="hr-lg centered">
    </div>
  </div>
</section>

<section class="image-grid pad-2-top pad-1-bottom">

  <div class="container image-container">

    <div class="row grid-row">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 grid-item">
        <img alt="place image description here" src="/images/ECC_Image1_students.png">
      </div>
      <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 grid-item">
        <img alt="place image description here" src="/images/EDCC_Image2_students.png">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 grid-item">
          <div class="text-overlay">
            <h5 class="lighter">Business:<br>Culinary Arts</h5>
            <hr class="hr-md light">
            <a href="#" class="btn btn-trans">Learn More</a>
          </div>
          <div class="style-overlay">
            <img alt="place image description here" src="/images/EDCC_Image3_students.png">
          </div>
      </div>
    </div>

    <div class="row grid-row">



      <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 grid-item">

          <img alt="place image description here" src="/images/EDCC_Image4_students.png">

      </div>

      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-lg-push-3 grid-item">
        <img alt="place image description here" src="/images/EDCC_Image6_students.png">
      </div>

      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-lg-pull-6 grid-item">
        <div class="text-overlay">
            <h5 class="lighter">Business:<br>Culinary Arts</h5>
            <hr class="hr-md light">
            <a href="#" class="btn btn-trans">Learn More</a>
          </div>
          <div class="style-overlay">
            <img alt="place image description here" src="/images/EDCC_Image5_students.png">
          </div>
      </div>

    </div>

    <div class="row grid-row">
      <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 grid-item">
        <img alt="place image description here" src="/images/EDCC_Image13_students.png">
      </div>
      <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 grid-item">
        <img alt="place image description here" src="/images/EDCC_Image14_students.png">
      </div>

      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 grid-item">
          <div class="text-overlay">
            <h5 class="lighter">Business:<br>Culinary Arts</h5>
            <hr class="hr-md light">
            <a href="#" class="btn btn-trans">Learn More</a>
          </div>
          <div class="style-overlay">
            <img alt="place image description here" src="/images/EDCC_Image8_students.png">
          </div>
      </div>

      <div class="hidden-xs col-sm-12 col-md-3 col-lg-3 grid-item">
        <img alt="place image description here" src="/images/EDCC_Image9_students.png">
      </div>
    </div>

    <div class="row grid-row">

      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 grid-item">
          <div class="text-overlay">
            <h5 class="lighter">Business:<br>Culinary Arts</h5>
            <hr class="hr-md light">
            <a href="#" class="btn btn-trans">Learn More</a>
          </div>
          <div class="style-overlay">
            <img alt="place image description here" src="/images/EDCC_Image10_students.png">
          </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 grid-item">
        <img alt="place image description here" src="/images/EDCC_Image11_students.png">
      </div>

      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 grid-item">
          <div class="text-overlay">
            <h5 class="lighter">Business:<br>Culinary Arts</h5>
            <hr class="hr-md light">
            <a href="#" class="btn btn-trans">Learn More</a>
          </div>
          <div class="style-overlay">
            <img alt="place image description here" src="/images/EDCC_Image12_students.png">
          </div>
      </div>

    </div>


  </div>
</section>

</div>

<?php include ('footer.php'); ?>