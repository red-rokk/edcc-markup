<section class="students-hero pad-4-bottom pad-0-top">

  <div class="jumbotron overlay-60">
    <img class="simple-image" src="/images/EDCC_Employee_Hero.jpg">
    
    <div class="container relative heading-centered">
        <div class="stu-overlay-content">
	        <div class="row head-comp">
	          <h1 class="text-centered lighter">Faculty &amp; Staff <span class="accent">Insider</span></h1>

	          <h2 class="margin-3-top margin-4-bottom text-centered lighter">Welcome to your student portal</h2>
	          <div class="text-centered student-quicklinks pad-4-both">
		          <div class="col-sm-12 col-md-4">
		          	<a href="#"><button class="btn btn-accent" type="button">Email Login</button></a>  
		          </div>
		          <div class="col-sm-12 col-md-4">
		          	<a href="#"><button class="btn btn-accent" type="button">TLR Login</button></a>  
		          </div>
		          <div class="col-sm-12 col-md-4">
		          	<a href="#"><button class="btn btn-accent" type="button">Employee Training</button></a>  
		          </div>
	          	
	          </div>      
	        </div> 
       	</div>     
       
    </div>      
  </div>

</section>