<section class="students-hero-internal pad-4-bottom pad-0-top">

  <div class="jumbotron overlay-40">
    <img class="simple-image" src="/images/EDCC_Hero_StudentSite.jpg" alt="EDCC Students">

    <div class="container relative heading-centered">
        <div class="stu-overlay-content">
	        <div class="row head-comp">
	          <h1 class="text-centered lighter">The Mighty, Mighty <span>Tritons</span></h1>

	          <h2 class="margin-3-top margin-4-bottom text-centered lighter">Welcome to your student portal</h2>

	        </div>
       	</div>

    </div>
  </div>

</section>