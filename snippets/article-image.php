

<section class="article-image pad-4-bottom">
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<img src="/images/EDCC_HospitalityTourismImage.jpg">
		</div>
	</div>
</section>


<section class="article-text-block pad-3-bottom ">
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12 margin-5-bottom">
			<div class="article-content">
		        <h4>LAUNCH AN EXCITING CAREER</h4>
		        <hr class="hr-lg dark">
		        	<p>Open up a world of career options - train to become an expert travel consultant, cruise or tour agent, tour guide, hotel staff member, or airline reservation and ticket agent. The hospitality and tourism industry is booming and our travel school program can make you a part of it all.</p>
		        	<br>
					<p>Because our classes are offered in the mornings, our program allows you to work part time or manage a family while attending school full time. Our program is home to a diverse student population; from recent high school graduates to adults changing careers as well as international students from all over the world.</p>
					<br>
					<p>Prepare to work in any part of the tourism industry by earning a one-year certificate, a two-year degree, or even a transfer degree, which would allow you to transfer to select university programs to earn a bachelor’s degree! Wherever you want to go, our Hospitality and Tourism program can get you there.</p>

		        	<a href="#" class="margin-5-top btn btn-primary">Apply Now</a>
		    </div>
		</div>
	</div>
</section>