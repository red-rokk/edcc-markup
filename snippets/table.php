<!-- S17: Table -->
<section class="edcc-table">
  <!-- Page Title -->

    <!-- Table -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="table-responsive">
              <table class="table " >
                <thead>
                  <tr>
                    <th>Name:</th>
                    <th>Title/Postion:</th>
                    <th>Office:</th>
                    <th>Phone:</th>
                    <th>Email:</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Andrew Angeli</td>
                    <td>PT Faculty (orchestral/mallets)</td>
                    <td>MIC 125</td>
                    <td>425.640.1339</td>
                    <td>staff@edcc.edu</td>
                  </tr>
                  <tr>
                    <td>Jameson Bratcher</td>
                    <td>PT Faculty (Low Brass)</td>
                    <td>MIC 125</td>
                    <td>425.640.1339</td>
                    <td>staff@edcc.edu</td>
                  </tr>
                  <tr>
                    <td>Willie Braun</td>
                    <td>PT Faculty (Cello)</td>
                    <td>MIC 125</td>
                    <td>425.640.1339</td>
                    <td>staff@edcc.edu</td>
                  </tr>
                  <tr>
                    <td>Debra DeMiero</td>
                    <td>PT Faculty (Piano)</td>
                    <td>MIC 125</td>
                    <td>425.640.1339</td>
                    <td>staff@edcc.edu</td>
                  </tr>
                  <tr>
                    <td>Jay Easton</td>
                    <td>PT Faculty (Clarinet/oboe)</td>
                    <td>MIC 125</td>
                    <td>425.640.1339</td>
                    <td>staff@edcc.edu</td>
                  </tr>
                  <tr>
                    <td>Stacey Eliason</td>
                    <td>PT Faculty (French Horn)</td>
                    <td>MIC 125</td>
                    <td>425.640.1339</td>
                    <td>staff@edcc.edu</td>
                  </tr>
                  <tr>
                    <td>Jacob W. Herbert</td>
                    <td>PT Faculty (Voice)</td>
                    <td>MIC 125</td>
                    <td>425.640.1339</td>
                    <td>staff@edcc.edu</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>





</section>
