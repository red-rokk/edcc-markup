<section class="image-grid pad-2-top pad-4-bottom">

  <div class="container image-container">

    <div class="row grid-row">
      <div class="hidden-xs col-sm-6 col-md-6 col-lg-6 grid-item">
        <img alt="Place image description here" src="/images/EDCC_Image1.png">
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 grid-item">
        <img alt="Place image description here" src="/images/EDCC_Image2.png">
      </div>
      <div class=" col-xs-12 col-sm-12 col-md-3 col-lg-3 grid-item">
          <div class="text-overlay">
            <h5 class="lighter">Business:<br>Culinary Arts</h5>
            <hr class="hr-md light">
            <a href="#" class="btn btn-trans">Learn More</a>
          </div>
          <div class="style-overlay">
            <img alt="Place image description here" src="/images/EDCC_Image2.png">
          </div>
      </div>
    </div>

    <div class="row grid-row">



      <div class="hidden-xs col-sm-6 col-md-3 col-lg-3 grid-item">
          <img alt="Place image description here" src="/images/EDCC_Image4.png">
      </div>

      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-lg-push-3 grid-item">
        <img alt="Place image description here" src="/images/EDCC_Image6.png">
      </div>

      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-lg-pull-6 grid-item">
        <div class="text-overlay">
            <h5 class="lighter">Business:<br>Anthropology</h5>
            <hr class="hr-md light">
            <a href="#" class="btn btn-trans">Learn More</a>
          </div>
          <div class="style-overlay">
            <img alt="Place image description here" src="/images/EDCC_Image5.png">
          </div>
      </div>

    </div>

    <div class="row grid-row">
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 grid-item">
        <img alt="Place image description here" src="/images/EDCC_Image7.png">
      </div>
      <div class="hidden-xs col-sm-6 col-md-3 col-lg-3 grid-item">
        <img alt="Place image description here" src="/images/EDCC_Image8.png">
      </div>

      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 grid-item">
          <div class="text-overlay">
            <h5 class="lighter">Business:<br>Visual Arts</h5>
            <hr class="hr-md light">
            <a href="#" class="btn btn-trans">Learn More</a>
          </div>
          <div class="style-overlay">
            <img alt="Place image description here" src="/images/EDCC_Image9.png">
          </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 grid-item">
        <img alt="Place image description here" src="/images/EDCC_Image10.png">
      </div>
    </div>

    <div class="row grid-row">

      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 grid-item">
          <div class="text-overlay">
            <h5 class="lighter">Business:<br>Marketing</h5>
            <hr class="hr-md light">
            <a href="#" class="btn btn-trans">Learn More</a>
          </div>
          <div class="style-overlay">
            <img alt="Place image description here" src="/images/EDCC_Image11.png">
          </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 grid-item">
        <img alt="Place image description here" src="/images/EDCC_Image12.png">
      </div>

      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 grid-item">
          <div class="text-overlay">
            <h5 class="lighter">Business:<br>Technology</h5>
            <hr class="hr-md light">
            <a href="#" class="btn btn-trans">Learn More</a>
          </div>
          <div class="style-overlay">
            <img alt="Place image description here" src="/images/EDCC_Image13.png">
          </div>
      </div>
      <div class="col-xs-12 visible-xs grid-item">
        <img alt="Place image description here" src="/images/EDCC_Image1.png">
      </div>

    </div>


  </div>
</section>