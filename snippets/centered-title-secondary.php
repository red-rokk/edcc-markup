<section class="header-hero pad-3-bottom">

  <div class="jumbotron brand-overlay-dark">

	  <div class="container stats-container">
	    <div class="row text-centered">
	        <h3 class="lighter">Centered Title Secondary Demo</h3>
	        <hr class="hr-lg centered light">
	    </div>
	  </div>
    <img class="simple-image" src="/images/EDCC_Stats_BGimage.jpg" alt="EDCC Backgriund image">
  </div>

</section>