<section class="section-feature pad-3-top pad-3-bottom">
  <div class="feature-container">

    <div class="hidden-lg hidden-md culture-image">
        <img class="" src="/images/EDCC_Main_TestimonialImage.jpg" alt="EDCC Main image">
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-4">
            </div>

            <div class="col-sm-12 col-md-6 col-lg-8">
                <div class="culture-call-right">
                    <h3 class="lighter">Triton <span>Stories</span></h3>
                    <hr class="hr-lg light">
                    <p class="lighter quote">“Edmonds Community College prepared me for a successful career in the culinary arts. Within a year of graduating, I became the head chef at one of Seattle’s top restaurants.”
                    <em>--Amanda Smith, EdCC Business Graduate</em>
                    </p>
                </div>
            </div>
        </div>
    </div>
  </div>
</section>