
 
<?php include ('../headers/head.php'); ?>
<?php include ('../headers/header-template.php'); ?>  

<div class="wrapper homepage" id="wrapper-index">

<section class="heading pad-3-top pad-2-bottom">
  <div class="container">
    <div class="row text-centered">
        <h3>Statistics Demo</h3>
        <hr class="hr-lg centered">
    </div>
  </div>
</section>	

<?php include ('../snippets/stats-banner.php'); ?>


</div><!-- Wrapper end -->

<?php include ('../footer.php'); ?>

