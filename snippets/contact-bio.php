<section class="contact-admin pad-5-top">

            <div class="row margin-2-both">
              <div class="col-xs-12">
                <h4>Contact Information</h4>
                <hr class="hr-lg dark">
              </div>
            </div>

            <!-- ForEach Search Result goes on each row -->
            <div class="row search-result">

              <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                <div class="circle-img-mask">
                  <img src="/images/contact-admin-headshot01.png">
                </div>
              </div>

              <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">

                  <h5 class="w700">Director of CSEL</h5>
                  <ul class="list-inline">
                    <li>Wayne Anthony</li>
                    <li><a href="tel:" class="sub-link">425.640.1578</a></li>
                    <li><a href="mailto:" class="sub-link">wanthony@edcc.edu</a></li>
                   </ul>

                <p class="margin-2-top margin-2-bottom hidden-xs">
                  Ask Me About: Campus Involvement, Leadership Development, Student Government, the Services and Activities Fee.
                </p>
                <hr class="hr-dark">
              </div>
            </div>

            <!-- ForEach Search Result goes on each row -->
            <div class="row search-result">

              <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                <div class="circle-img-mask">
                  <img src="/images/contact-admin-headshot01.png">
                </div>
              </div>

              <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">

                  <h5 class="w700">ASSITANT DIRECTOR FOR CSEL</h5>
                  <ul class="list-inline">
                    <li>Wayne Anthony</li>
                    <li><a href="tel:" class="sub-link">425.640.1578</a></li>
                    <li><a href="mailto:" class="sub-link">wanthony@edcc.edu</a></li>
                   </ul>

                <p class="margin-2-top margin-2-bottom hidden-xs">
                  Ask Me About: Campus Involvement, Leadership Development, Student Government, the Services and Activities Fee.
                </p>
                <hr class="hr-dark">
              </div>
            </div>

            <!-- ForEach Search Result goes on each row -->
            <div class="row search-result">

              <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                <div class="circle-img-mask">
                  <img src="/images/contact-admin-headshot01.png">
                </div>
              </div>

              <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">

                  <h5 class="w700">ASSITANT DIRECTOR FOR Student engagement</h5>
                  <ul class="list-inline">
                    <li>Wayne Anthony</li>
                    <li><a href="tel:" class="sub-link">425.640.1578</a></li>
                    <li><a href="mailto:" class="sub-link">wanthony@edcc.edu</a></li>
                   </ul>

                <p class="margin-2-top margin-2-bottom hidden-xs">
                  Ask Me About: Campus Involvement, Leadership Development, Student Government, the Services and Activities Fee.
                </p>
                <hr class="hr-dark">
              </div>
            </div>

            <!-- ForEach Search Result goes on each row -->
            <div class="row search-result">

              <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                <div class="circle-img-mask">
                  <img src="/images/contact-admin-headshot01.png">
                </div>
              </div>

              <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">

                  <h5 class="w700">PROGRAM MANAGER FOR DIVERSITY CENTER</h5>
                  <ul class="list-inline">
                    <li>Wayne Anthony</li>
                    <li><a href="tel:" class="sub-link">425.640.1578</a></li>
                    <li><a href="mailto:" class="sub-link">wanthony@edcc.edu</a></li>
                   </ul>

                <p class="margin-2-top margin-2-bottom hidden-xs">
                  Ask Me About: Campus Involvement, Leadership Development, Student Government, the Services and Activities Fee.
                </p>
                <hr class="hr-dark">
              </div>
            </div>

            <!-- ForEach Search Result goes on each row -->
            <div class="row search-result">

              <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                <div class="circle-img-mask">
                  <img src="/images/contact-admin-headshot01.png">
                </div>
              </div>

              <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">

                  <h5 class="w700">Director of CSEL</h5>
                  <ul class="list-inline">
                    <li>Wayne Anthony</li>
                    <li><a href="tel:" class="sub-link">425.640.1578</a></li>
                    <li><a href="mailto:" class="sub-link">wanthony@edcc.edu</a></li>
                   </ul>

                <p class="margin-2-top margin-2-bottom hidden-xs">
                  Ask Me About: Campus Involvement, Leadership Development, Student Government, the Services and Activities Fee.
                </p>
                <hr class="hr-dark">
              </div>
            </div>

          </section>