

<!--Header Employee-->

<header class="navbar-fixed-top header-employee">
    <!--Navbar-->


<nav class="navbar navbar-fixed-top" role="navigation">
    <a href="#wrapper-index" class="sr-only">Skip Navigation to Page Content</a>

    <div class="offcanvas-navbar brand-bg-secondary second fixed-load hidden-md-down">
        <div class="container">
            <div class="row no-gutter row-offcanvas">
                <div class="navbar-header page-scroll">



                    <a class="navbar-toggle edcc-mobile" data-toggle="offcanvas"><i class="fa fa-cog" aria-hidden="true"></i> My EDCC <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>

                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex2-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                </div>

                <div class="collapse navbar-collapse navbar-ex2-collapse">
                    <ul class="pull-left nav navbar-nav">
                        <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                        <li class="hidden">
                            <a class="" href="#"></a>
                        </li>
                        <li class="nav-item">
                            <a class="" href="/index.php"><i class="fa fa-home" aria-hidden="true"></i> Main</a>
                        </li>
                        <li class="nav-item">
                            <a class="" href="/students.php"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Students</a>
                        </li>
                        <li class="nav-item active">
                            <a class="" href="/employee.php"><i class="fa fa-briefcase" aria-hidden="true"></i> Employees</a>
                        </li>
                        <li class="nav-item">
                            <a class="" href="#"><i class="fa fa-globe" aria-hidden="true"></i> International</a>
                        </li>
                    </ul>


                    <ul class="pull-right nav navbar-nav">
                        <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                        <li class="hidden active">
                            <a class="page-scroll" href="#"></a>
                        </li>
                        <li class="nav-item">
                            <a class="brand-item-em text-centered" href="#">Canvas</a>
                        </li>
                        <li class="nav-item">
                            <a class="myedcc-mobile" data-toggle="offcanvas"><i class="brand-color-tertiary fa fa-cog" aria-hidden="true"></i> My EDCC  <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

    <div class="logo-navbar second fixed-load">
        <div class="container">

            <div class="navbar-header page-scroll">

                <a class="navbar-brand" href="#">
                    <img src="/images/EDCC_Main_Logo_color.png" alt="EDCC Home Page">
                </a>
            </div>
        </div>
    </div>


    <div class="dropdown-navbar second fixed-load">
        <div class="container">
            <div class="row no-gutter row-offcanvas">
                <div class="navbar-header page-scroll">
                    <h5>Main Menu</h5>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex3-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <?php include('header-parts/employee-dropdown-menu.php'); ?>

            </div>
        </div>
    </div>


</nav>


</header>


<?php include ('off_canvas_nav.php'); ?>





