
  
<?php include ('headers/head.php'); ?>
<?php include ('headers/header-main.php'); ?>  

<div class="wrapper homepage" id="wrapper-index"> 


<?php include ('snippets/homepage-hero.php'); ?>
  
<section class="heading pad-5-top pad-2-bottom">
  <div class="container">
    <div class="row text-centered">
        <h3>WELCOMING. DIVERSE. INNOVATIVE.</h3>
        <hr class="hr-lg centered">
    </div>
  </div>
</section>  
  

<?php include ('snippets/text-block-centered.php'); ?>
<?php include ('snippets/stats-banner.php'); ?>

<section class="heading pad-2-top pad-2-bottom">
  <div class="container">
    <div class="row text-centered">
        <h3>Areas of Study</h3>
        <hr class="hr-lg centered">
    </div>
  </div>
</section>  

<?php include ('snippets/image-grid.php'); ?>   
<?php include ('snippets/testimonial.php'); ?>

<section class="heading pad-3-top pad-3-bottom">
  <div class="container">
    <div class="row text-centered">
        <h3>News and Events</h3>
        <hr class="hr-lg centered">
    </div>
  </div>
</section> 

<?php include ('snippets/news-events.php'); ?>





</div><!-- Wrapper end -->

<?php include ('footer.php'); ?>

