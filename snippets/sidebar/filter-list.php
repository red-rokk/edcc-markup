<section class="sidebar">
            <div class="post-box">
              <div class="pad-3-bottom">
                <h5>Hospitality/Tourism</h5>
                <hr class="hr-sm dark">
                <ul class="list-unstyled list-group">
                  <li class="list-group-item"><a href="#">Course Descriptions/Objectives</a></li>
                  <li class="list-group-item"><a href="#">Degrees and Certificates</a></li>
                  <li class="list-group-item"><a href="#">For Placement Only</a></li>
                 
                </ul>
              </div>
              <div class="pad-3-bottom">
                <h5>For more information</h5>
                <hr class="hr-sm dark">
                <ul class="list-unstyled list-group">
	                <li class="list-group-item subtitle">Open Program Advising</li>
	                <li class="list-group-item">2/28 - 1:30 - 3 PM</li>
	                <li class="list-group-item">3/7: 1:30 - 3 PM</li>
	                <li class="list-group-item">Snohomish Hall Lobby</li>  
	                <li class="list-group-item subtitle">Schedule an Appointment</li>
	                <li class="list-group-item">Beth O’Donnel<br><a class="email" href="#">bodonnel@edcc.edu</a></li>   
	                <li class="list-group-item">Linda Russell<br><a class="email" href="#">lrussell@edcc.edu</a></li>
	                <li class="list-group-item subtitle">Contact Advising Department</li>
	                <li class="list-group-item"><a class="email" href="#">425.640.1458</a></li>
                 
                </ul>
              </div>
            </div>

    </section>