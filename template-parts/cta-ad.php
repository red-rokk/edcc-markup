
 
<?php include ('../headers/head.php'); ?>
<?php include ('../headers/header-template.php'); ?>  

<div class="wrapper homepage" id="wrapper-index">

<section class="heading pad-3-top pad-2-bottom">
  <div class="container">
    <div class="row text-centered">
        <h3>Call to Action Sidebar Demo</h3>
        <hr class="hr-lg centered">
    </div>
  </div>
</section> 


  <div class="container">
	<div class="row">
		<div class="two-third col-xs-12 col-sm-12 col-md-8 col-lg-8"> <!-- Two thirds Section-->	
			

		<section class="article-text-block pad-3-bottom">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12">
					<div class="article-content">
				        <h4>LAUNCH AN EXCITING CAREER</h4>
				        <hr class="hr-lg dark">
				        	<p>Open up a world of career options - train to become an expert travel consultant, cruise or tour agent, tour guide, hotel staff member, or airline reservation and ticket agent. The hospitality and tourism industry is booming and our travel school program can make you a part of it all.</p>
				        	<br>
							<p>Because our classes are offered in the mornings, our program allows you to work part time or manage a family while attending school full time. Our program is home to a diverse student population; from recent high school graduates to adults changing careers as well as international students from all over the world.</p>
							<br>
							<p>Prepare to work in any part of the tourism industry by earning a one-year certificate, a two-year degree, or even a transfer degree, which would allow you to transfer to select university programs to earn a bachelor’s degree! Wherever you want to go, our Hospitality and Tourism program can get you there.</p>
					</div>
  
				</div>
			</div>
		</section>

		</div> <!-- EO Two thirds Section-->
		      

		<div class="one-third col-xs-12 col-sm-12 col-md-4 col-lg-4"> <!-- One third Section-->
		
		<?php include ('../snippets/sidebar/sidebar-submit-post.php'); ?>	
		<?php include ('../template-sidebar.php'); ?>
			

		</div> <!-- EO One third Section-->

	</div>
</div>

</div><!-- Wrapper end -->

<?php include ('../footer.php'); ?>

