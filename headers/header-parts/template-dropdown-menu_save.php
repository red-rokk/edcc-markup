<div class="collapse navbar-collapse navbar-ex3-collapse">

  <ul class="nav navbar-nav">
      <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
      <li class="hidden active">
          <a></a>
      </li>



      <li class="nav-item dropdown">
          
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Hero Banners</a>

          <ul class="dropdown-menu">
             
            <div class="row multicol-nav">
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="/template-parts/homepage.php">Home Page</a></li>
                   <li><a class="dropdown-item" href="/template-parts/students-page.php">Students Page</a></li>
                   <li><a class="dropdown-item" href="/template-parts/employees-page.php">Employees Page</a></li>
                   

                </ul>       
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="/template-parts/students-internal.php">Students Internal</a></li>
                   <li><a class="dropdown-item" href="/template-parts/employees-internal.php">Employees Internal</a></li>
                   <li><a class="dropdown-item" href="/template-parts/registration.php">Registration Login</a></li>
                </ul>
            </div>
             
          </ul>
      </li> 


      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Full Width Page</a>
          <ul class="dropdown-menu">
             
            <div class="row multicol-nav">
                <ul class="list-unstyled col-md-6">
                   <li><a class="dropdown-item" href="/template-parts/centered-title-primary.php">Centered Title Primary</a></li>
                   <li><a class="dropdown-item" href="/template-parts/centered-title-secondary.php">Centered Title Secondary</a></li>
                   <li><a class="dropdown-item" href="/template-parts/statistics.php">Statistics Section</a></li>
                   <li><a class="dropdown-item" href="/template-parts/image-grid.php">Image Grid</a></li>
                   <li><a class="dropdown-item" href="/template-parts/tables.php">Tables</a></li>
                   

                </ul>  
                <ul class="list-unstyled col-md-6">
                   
                   <li><a class="dropdown-item" href="/template-parts/testimonial.php">Testimonial Section</a></li>
                   <li><a class="dropdown-item" href="/template-parts/news-events.php">News &amp; Events</a></li>
                   <li><a class="dropdown-item" href="/template-parts/dates-events.php">Dates &amp; Events</a></li>
                   <li><a class="dropdown-item" href="/template-parts/featured-cards.php">Featured Cards</a></li>

                </ul>     

            </div>
             
          </ul>
      </li>

      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Sidebar Page</a>
          <ul class="dropdown-menu">
             
            <div class="row multicol-nav">  
                <ul class="list-unstyled col-md-6">
                   <li class="w900 dropdown-item">2/3 Boxed Container</li>
                   <li><a class="dropdown-item" href="/template-parts/community-board-loop.php">Community Board Loop</a></li>
                   <li><a class="dropdown-item" href="/template-parts/community-board-internal.php">Community Board Internal</a></li>
                   <li><a class="dropdown-item" href="/template-parts/contact-bio.php">Contact Bio List</a></li>
                   <li><a class="dropdown-item" href="/template-parts/accordion.php">Accordion</a></li>
                   <li><a class="dropdown-item" href="/template-parts/article-image.php">Article Image</a></li>
                   <li><a class="dropdown-item" href="/template-parts/article-video.php">Article Video</a></li>
                   <li><a class="dropdown-item" href="/template-parts/google-results.php">Google Search Results</a></li>
                   <li><a class="dropdown-item" href="/template-parts/class-results.php">Class Search Results</a></li>
                 

                </ul>       
                <ul class="list-unstyled col-md-6">
                   <li class="w900 dropdown-item">1/3 Sidebar Container</li>
                   <li><a class="dropdown-item" href="/template-parts/filter-list.php">Filter List</a></li>
                   <li><a class="dropdown-item" href="/template-parts/filter-search.php">Filter Search</a></li>
                   <li><a class="dropdown-item" href="/template-parts/filter-button.php">Filter Button</a></li>
                   <li><a class="dropdown-item" href="/template-parts/cta-ad.php">Call to Action Ad</a></li>
                   <li><a class="dropdown-item" href="/template-parts/facebook-feed.php">Facebook Feed</a></li>
                   <li><a class="dropdown-item" href="/template-parts/sharing-icons.php">Sharing Icons</a></li>
                </ul>
            </div>
             
          </ul>
      </li>


      <li class="hidden-xs nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search" aria-hidden="true"></i></a>
          <ul class="dropdown-menu search-menu">
            <div class="row multicol-nav">
              <form role="search" method="get" id="searchform" action="" _lpchecked="1">
                  <div class="input-group">
                      <input type="text" class="field form-control" name="s" id="s" placeholder="Search …">
                      <span class="input-group-btn">
                          <input type="submit" class="submit btn btn-primary" name="submit" id="searchsubmit" value="Search">
                      </span>
                   </div>
              </form>
            </div>

          </ul>
      </li>
      <li class="visible-xs nav-item xs-search">    
        <form role="search" method="get" id="searchform" action="" _lpchecked="1">
            <div class="input-group">
                <input type="text" class="field form-control" name="s" id="s" placeholder="Search …">
             </div>
        </form>   
      </li>

  </ul>
</div>